define(['components/userNav/assets/scripts/currencyList','proclaim'], function (currencyList, proclaim) {

    describe('currency list', function () {

        beforeEach(function () {

            var html = '<div class="currency-list-fixture">' +
                '<ul class="currency"></ul>' +
                '</div>';

            $('body').append(html);

        });

        afterEach(function () {
            $('.currency-list-fixture').remove();
        });

        it('should add currencies ul to dom when executed', function () {

            currencyList.init();

            var length = $('.currency-list-fixture li').length;
            proclaim.equal(length, 88);

        });

        it('should include currency name and data attribute with currency code', function () {

            currencyList.init();

            var currency = $('.currency-list-fixture li:eq(0)').data('curr');
            proclaim.equal(currency, 'AUD');
            var text = $('.currency-list-fixture li:eq(0)').text();
            proclaim.equal(text, 'Australia Dollars');

        });

    });

});
