define(['proclaim', 'components/userNav/assets/scripts/personalisation'], function (proclaim, personalisation) {
    'use strict';

    describe('locale and currency selector', function () {

        var cookieName = '',
            cookieValue = '',
            cookieOptions = '';

        var fakeCookie = function (name, val, options) {
            cookieName = name;
            cookieValue = val;
            cookieOptions = options;
        };

        var fakeWindow = {
            location: undefined
        };

        describe('locale selector', function () {

            beforeEach(function () {
                cookieName = '';
                cookieValue = '';
                cookieOptions = '';

                _gaq = [];

                $('body').append('<div class="fixture">' +
                    '<ul class="locale">' +
                    '  <li data-locale="EN"><a href="/en/">english</a>/li>' +
                    '  <li data-locale="ES"><a href="/es/">spanish</a></li>' +
                    '</ul>' +
                    '  <ul class="languages">' +
                    '    <li>Other Sites:</li>' +
                    '    <li data-locale="FR">' +
                    '      <a href="/fr/">Français</a>' +
                    '    </li>' +
                    '  </ul>' +
                    '</div>');

                personalisation.init(fakeCookie, fakeWindow);
            });

            afterEach(function () {
                $('.fixture').remove();
            });

            it('should set a cookie value when an option is selected', function () {
                $('.fixture li:eq(1) a').click();
                proclaim.equal(JSON.parse(cookieValue).languagecode, 'ES');
                proclaim.equal(JSON.parse(cookieValue).setbyuser, true);
            });

            it('should set the name of the cookie to LateRoomsProfile', function () {
                $('.fixture li:eq(1) a').click();
                proclaim.equal(cookieName, 'LateroomsProfile');
            });

            it('should set expiry to thirty days and path to root', function () {
                $('.fixture li:eq(1) a').click();
                proclaim.deepEqual(cookieOptions, {
                    expires: 30,
                    path: '/'
                });
            });

            it('should default other options to en and gbp', function () {
                var expectedValue = JSON.stringify({
                    languagecode: 'ES',
                    countrycode: 'GB',
                    currencycode: 'GBP',
                    setbyuser: true
                });

                $('.fixture li:eq(1) a').click();
                proclaim.equal(cookieValue, expectedValue);
            });

            it('should redirect to locale homepage', function () {
                $('.fixture li:eq(1) a').click();
                proclaim.equal(fakeWindow.location, '/');
            });

            it('should log locale in analytics on click', function () {
                $('.fixture li:eq(1) a').click();

                var gaqValue = _gaq[0];
                proclaim.equal(gaqValue[0], '_trackEvent');
                proclaim.equal(gaqValue[1], 'Site Version');
                proclaim.equal(gaqValue[2], 'ES/GB/GBP');
            });

            it('should log footer language link clicks in analytics', function () {
                $('ul.languages li a').trigger('click');

                var gaqValue = _gaq[0];

                proclaim.equal(gaqValue[0], '_trackEvent');
                proclaim.equal(gaqValue[1], 'HomePage');
                proclaim.equal(gaqValue[2], 'Footer');
                proclaim.equal(gaqValue[3], 'Other Site - Français');
            });
        });

        describe('currency selector', function () {
            beforeEach(function () {
                cookieName = '';
                cookieValue = '';
                cookieOptions = '';

                _gaq = [];

                $('body').append('<div class="fixture">' +
                    '<ul class="currency">' +
                    '<li data-curr="GBP">Great British Pounds</li>' +
                    '<li data-curr="EUR">Euro</li>' +
                    '</ul>' +
                    '</div>');

                personalisation.init(fakeCookie, fakeWindow);
            });

            afterEach(function () {
                $('.fixture').remove();
            });

            it('should set a cookie value when an option is selected', function () {
                $('.fixture li:eq(1)').click();
                proclaim.equal(JSON.parse(cookieValue).currencycode, 'EUR');
            });

            it('should log currency in analytics on click', function () {
                $('.fixture li:eq(1)').click();

                var gaqValue = _gaq[0];
                proclaim.equal(gaqValue[0], '_trackEvent');
                proclaim.equal(gaqValue[1], 'Site Version');
                proclaim.equal(gaqValue[2], 'EN/GB/EUR');
            });

        });
    });
});
