define(function () {
    'use strict';

    return {
        init: function (cookie, windowObj) {

            var cookiePreferences = {
                languagecode: 'EN',
                countrycode: 'GB',
                currencycode: 'GBP'
            };

            $('.locale li a').on('click', function () {
                cookiePreferences.languagecode = $(this).parent().data('locale');
                cookiePreferences.setbyuser = true;
                var siteVersion = cookiePreferences.languagecode + '/' + cookiePreferences.countrycode + '/' + cookiePreferences.currencycode;
                _gaq.push(['_trackEvent', 'Site Version', siteVersion], updateCookie());
            });

            $('.languages li a').on('click', function () {
                var text = $(this).text();
                var href = $(this).attr('href');
                cookiePreferences.languagecode = $(this).parent().data('locale');
                cookiePreferences.setbyuser = true;
                _gaq.push(['_trackEvent', 'HomePage', 'Footer', 'Other Site - ' + text], updateCookie(href));
            });

            $('.currency li').on('click', function () {
                cookiePreferences.currencycode = $(this).data('curr');
                var siteVersion = cookiePreferences.languagecode + '/' + cookiePreferences.countrycode + '/' + cookiePreferences.currencycode;
                _gaq.push(['_trackEvent', 'Site Version', siteVersion], updateCookie());
            });

            function updateCookie(href) {
                cookie('LateroomsProfile', JSON.stringify(cookiePreferences), {
                    expires: 30,
                    path: '/'
                });
                if (href && href.length > 0) {
                    windowObj.location = href;
                } else {
                    windowObj.location = '/';
                }
            }
        }
    };
});
