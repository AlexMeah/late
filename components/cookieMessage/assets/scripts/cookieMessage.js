define(['jquerycookie'], function () {

    return function initCookieMessage() {

        var $cookieMsg = $('.cookie-msg');

        if ($.cookie('cookieMessageShown') === 'shown') {
            return;
        }
        else {
            $cookieMsg.removeClass('hidden');
        }

        $('.cookie-close').on('click', function (e) {
            e.preventDefault();
            $cookieMsg.addClass('hidden');
        });

        $(document).on('click', function () {
            $.cookie('cookieMessageShown', 'shown', { expires: 2000, path: '/' });
        });

        $('.cookie-info').on('click', function (e) {
            e.stopPropagation();
        });

    };

});
