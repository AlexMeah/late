define(['proclaim', 'components/cookieMessage/assets/scripts/cookieMessage'], function (proclaim, cookieMessage) {
    describe('cookie message', function () {

        var cookieName = '',
            cookieValue = '',
            cookieOptions = '';

        beforeEach(function () {

            var html = '<section class="cookie-msg hidden">' +
                            '<a class="cookie-info" href="#">More info</a>' +
                            '<a class="cookie-close" href="#">X</a>' +
                        '</section>';

            $('body').append(html);

            $.cookie('cookieMessageShown','');
        });

        afterEach(function () {

            cookieName = '';
            cookieValue = '';
            cookieOptions = '';

        });

        it('should display if cookie doesnt exist.', function () {

            cookieMessage();
            var messageIsHidden = $('.cookie-msg').hasClass('hidden');
            proclaim.isFalse(messageIsHidden);

        });

        it('should set a cookie on any click event.', function () {

            cookieMessage();

            $(document).click();

            proclaim.equal($.cookie('cookieMessageShown'), 'shown');
//            proclaim.deepEqual(cookieOptions, { expires: 2000, path: '/' });

        });

        it('should not set a cookie when .cookie-info is clicked.', function () {

            cookieMessage();

            $('.cookie-info').click();

            proclaim.equal(cookieName, '');
        });

        it('should be hidden if cookie is set.', function () {
            $.cookie('cookieMessageShown', 'shown');
            cookieMessage();

            var cookieIsHidden = $('.cookie-msg').hasClass('hidden');
            proclaim.isTrue(cookieIsHidden);
        });

        it('should close the message if the close element is clicked', function () {
            cookieMessage();

            var messageIsHidden = $('.cookie-msg').hasClass('hidden');
            proclaim.isFalse(messageIsHidden);

            $('.cookie-msg .cookie-close').click();
            messageIsHidden = $('.cookie-msg').hasClass('hidden');
            proclaim.isTrue(messageIsHidden);
        });
    });
});
