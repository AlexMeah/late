var getView = require('../../../lib/getView');

module.exports = function (parameters, callback) {
    getView('loadBalancerCheck', function (err, view) {
        callback(err, view);
    });
};
