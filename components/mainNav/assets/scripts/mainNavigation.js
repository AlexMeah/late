define(function () {
    'use strict';

    return function () {

        $('.container ul li.dd ul').on('touchstart', 'li', function (e) {
            e.stopPropagation();
        });

        $('.container ul li.dd').on('click', function (e) {
            if (!$('body').hasClass('no-touch') || $('.menu').is(':visible')) {
                e.stopPropagation();
                if ($(this).hasClass('active')) {
                    $('.container ul li.dd, .main-nav, .user-nav').removeClass('active');
                } else {
                    $('.container ul li.dd, .main-nav, .user-nav').removeClass('active');
                    $(this).addClass('active');

                    if ($('.main').hasClass('active') && $(this).parents('.user-nav').length) {
                        $('.main-nav, .user-nav').addClass('active');
                    }
                }
            }
        });

        $('body').on('touchstart', function (e) {
            e.stopPropagation();
            if (!$('.main').hasClass('active')) {
                $('.container ul li.dd, .main-nav, .user-nav').removeClass('active');
            }
        });

        $('.menu').on('click', function (e) { /* touchstart issue, touch propogates! */
            e.stopPropagation();
            $('.main').toggleClass('active');
            $('.container ul li.dd, .main-nav, .user-nav').removeClass('active');
        });

        $('.main-nav ul li a').on('click', function (e) {
            e.preventDefault();

            var $this = $(this);
            var text = $this.html();
            var location = $this.attr('href');

            //find the parent
            var parent = $this.parents('.main-nav li.dd').first();

            var parentText = parent.clone().children().remove().end().text();

            parentText = $.trim(parentText);

            if (parentText.length > 0) {
                text = parentText + ' > ' + text;
            }

            _gaq.push(['_trackEvent', 'HomePage', 'Navigation', text], function () {
                if (location) {
                    document.location = location;
                }
            });
        });

    };
});
