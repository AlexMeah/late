define(['proclaim', 'components/mainNav/assets/scripts/mainNavigation'], function (proclaim, mainNavigation) {

    describe('Navigation', function () {

        beforeEach(function () {
            _gaq = [];

            var html =
                '<div class="fixture">' +
                '  <div class="menu"></div>' +
                '    <section class="main"></section> ' +
                '    <div class="container">' +
                '      <nav class="main-nav">' +
                '        <ul>' +
                '          <li class="dd">menu 1' +
                '            <ul>' +
                '              <li>' +
                '                <a href="#">Sub Link 1</a>' +
                '              </li>' +
                '              <li>' +
                '                <a href="#">Sub Link 2</a>' +
                '              </li>' +
                '            </ul>' +
                '          </li>' +
                '          <li class="dd">menu 2' +
                '            <ul></ul>' +
                '          </li>' +
                '        </ul>' +
                '        <ul>' +
                '          <li>' +
                '            <a href="#">Link</a>' +
                '          </li>' +
                '        </ul>' +
                '     </nav>' +
                '     <nav class="user-nav">' +
                '       <ul>' +
                '         <li class="dd">menu 1' +
                '           <ul></ul>' +
                '         </li>' +
                '         <li class="dd">menu 2' +
                '           <ul></ul>' +
                '         </li>' +
                '       </ul>' +
                '     </nav>' +
                '   </div>' +
                '</div>';

            $('body').append(html);
            mainNavigation();
        });

        afterEach(function () {
            $('.fixture').remove();
        });

        describe('Mobile', function () {

            describe('LONG VIEW', function () {

                it('should display submenu when clicked - usernav/mainnav', function () {
                    $('.main-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.main-nav > ul > li:nth-child(1)').hasClass('active'), true);
                });

                it('should hide submenu when cliked and already open - usernav/mainnav', function () {
                    $('.main-nav > ul > li:nth-child(1)').trigger('click');
                    $('.main-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.main-nav > ul > li:nth-child(1)').hasClass('active'), false);
                });

                it('should hide previously shown submenu when new one is cliked - usernav/mainnav', function () {
                    $('.main-nav > ul > li:nth-child(1)').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.main-nav > ul > li:nth-child(1)').hasClass('active'), false);
                    proclaim.equal($('.user-nav > ul > li:nth-child(1)').hasClass('active'), true);
                });

                it('should hide active submenu when anywhere in the document is clicked - usernav/mainnav', function () {
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    $('body').trigger('touchstart');
                    proclaim.equal($('.user-nav > ul > li:nth-child(1)').hasClass('active'), false);
                });

            });

            describe('SIDE VIEW', function () {

                it('should display mobile nav when menu icon is clicked', function () {
                    $('.menu').trigger('click');
                    proclaim.equal($('.main').hasClass('active'), true);
                });

                it('should hide mobile nav when  menu icon is clicked again', function () {
                    $('.menu').trigger('click');
                    $('.menu').trigger('click');
                    proclaim.equal($('section.main').hasClass('active'), false);
                });

                it('should display submenu when clicked in mobile nav- usernav', function () {
                    $('.menu').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.user-nav > ul > li:nth-child(1)').hasClass('active'), true);
                });

                it('should toggle submenus - usernav', function () {
                    $('.menu').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    $('.user-nav > ul > li:nth-child(2)').trigger('click');
                    proclaim.equal($('.user-nav > ul > li:nth-child(1)').hasClass('active'), false);
                    proclaim.equal($('.user-nav > ul > li:nth-child(2)').hasClass('active'), true);
                });

                /** fix after release **/
                // it('should toggle submenus - usernav', function() {
                //     $('.menu').trigger('click');
                //     $('.user-nav > ul > li:nth-child(1)').trigger('click');
                //     $('.main-nav > ul > li:nth-child(1)').trigger('click');
                //     $('.user-nav > ul > li:nth-child(1)').hasClass('active'), true);
                // });

                it('should close all open submenus when mobile nav is closed', function () {
                    $('.menu').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.user-nav > ul > li:nth-child(1)').hasClass('active'), true);
                    $('.menu').trigger('click');
                    proclaim.equal($('.user-nav > ul > li:nth-child(1)').hasClass('active'), false);
                });

                it('should push down main nav when any of the usernav is toggled', function () {
                    $('.menu').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.main-nav').hasClass('active'), true);
                });

                it('should pull up main nav when any of the usernav is toggled', function () {
                    $('.menu').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    $('.user-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.main-nav').hasClass('active'), false);
                });

            });

        });

        describe('Desktop', function () {

            beforeEach(function () {
                $('body').addClass('no-touch');
                $('.menu').hide();
            });

            describe('LONG VIEW', function () {
                it('should NOT display submenu on click - usernav/mainnav', function () {
                    $('.main-nav > ul > li:nth-child(1)').trigger('click');
                    proclaim.equal($('.main-nav > ul > li:nth-child(1)').hasClass('active'), false);
                });

            });

        });

        describe('Analytics', function () {
            it('should log top level menu item in analytics on click', function () {
                $('.main-nav ul:nth-child(2) li a').trigger('click');

                var gaqValue = _gaq[0];
                proclaim.equal(gaqValue[0], '_trackEvent');
                proclaim.equal(gaqValue[1], 'HomePage');
                proclaim.equal(gaqValue[2], 'Navigation');
                proclaim.equal(gaqValue[3], 'Link');
            });

            it('should log top level menu item with its sub menu item in analytics on click', function () {
                $('.main-nav > ul:first-child > li:first-child > ul > li:first-child a').trigger('click');

                var gaqValue = _gaq[0];
                proclaim.equal(gaqValue[0], '_trackEvent');
                proclaim.equal(gaqValue[1], 'HomePage');
                proclaim.equal(gaqValue[2], 'Navigation');
                proclaim.equal(gaqValue[3], 'menu 1 > Sub Link 1');
            });
        });

    });
});
