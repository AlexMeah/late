define(function () {
    return function () {

        window.___gcfg = {
            lang: 'en-UK'
        };
        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js?onload=onLoadCallback';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = '//connect.facebook.net/en_GB/all.js#xfbml=1&appId=138871299465082';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $('.social-media a').on('click', function (e) {
            e.preventDefault();
            var _this = this;
            var socialTitle = $(_this).attr('title');
            _gaq.push(['_trackEvent', 'HomePage', 'Social Interactions', socialTitle], function () {
                document.location = $(_this)[0].href;
            });
        });

        window.fbAsyncInit = function () {
            FB.Event.subscribe('edge.create', function (targetUrl) {
                _gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
            });
            FB.Event.subscribe('edge.remove', function (targetUrl) {
                _gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
            });
            FB.Event.subscribe('message.send', function (targetUrl) {
                _gaq.push(['_trackSocial', 'facebook', 'send', targetUrl]);
            });
        };
    };
});
