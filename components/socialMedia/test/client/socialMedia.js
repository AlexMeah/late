define(['proclaim', 'components/socialMedia/assets/scripts/socialMedia'], function (proclaim, socialMedia) {
    describe('social media', function () {
        beforeEach(function () {

            _gaq = [];

            var html =
                '<section class="social-media">' +
                '  <ul>' +
                '    <li>' +
                '      <a href="https://www.test.com" title="Test Title">' +
                '        <img src="images/social-media/test.png" height="40" width="40" alt="Test Alt">' +
                '     </a>' +
                '  </li>' +
                '</section>';

            $('body').append(html);

            socialMedia();

        });

        it('should log social click in analytics', function () {

            $('.social-media a').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Social Interactions');
            proclaim.equal(gaqValue[3], 'Test Title');
        });
    });
});
