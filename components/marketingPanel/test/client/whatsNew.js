define(['proclaim', 'components/marketingPanel/assets/scripts/whatsNew'], function (proclaim, whatsNew) {
    describe('Whats New', function () {

        beforeEach(function () {

            _gaq = [];

            var html =
                '<section class="whats-new">' +
                '  <article class="story">' +
                '    <a href="http://www.test.com" title="Test Title" id="whatsNewId">' +
                '      <img width="470" height="266" alt="Test Alt">' +
                '    </a>' +
                '  </article>' +
                '</section>';
            $('body').append(html);

            whatsNew();
        });

        afterEach(function () {
            $('.fixture').remove();
        });

        it('should log clicks in analytics', function () {
            $('.whats-new a').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Whats New');
            proclaim.equal(gaqValue[3], 'whatsNewId');
        });
    });
});
