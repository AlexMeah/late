define(function () {
    return function () {
        $('.whats-new a').on('click', function (e) {
            e.preventDefault();
            var _this = this;

            var whatsNewId = $(_this).attr('id');
            _gaq.push(['_trackEvent', 'HomePage', 'Whats New', whatsNewId],
                function () {
                    document.location = $(_this)[0].href;
                }
            );
        });
    };
});
