// var async = require('async');
// var handlebars = require('handlebars');
// var getView = require('../../../lib/getView');
// var asyncObjectMap = require('../../../lib/utils/asyncObjectMap');
// var loadLayout = async.apply(getView, 'layout');

// var renderSlot = function renderSlot(slotConfig, callback) {

//     var getComponent = require('../../../lib/components');
//     var componentsForSlot = slotConfig.components;

//     var renderComponent = function renderComponent(componentParameters, callback) {
//         getComponent(componentParameters.type, function (err, component) {
//             if (err) {
//                 return callback(err);
//             }

//             component(componentParameters, callback);
//         });
//     };

//     async.map(componentsForSlot, renderComponent, function combineComponentOutput(err, outputFromEachComponentInSlot) {
//         if (err) {
//             return callback(err);
//         }

//         var combinedHtmlForSlot = outputFromEachComponentInSlot.join('');

//         callback(err, combinedHtmlForSlot);
//     });
// };

// module.exports = function (parameters, callback) {
//     console.log('\n\n\n\n\n\n\n' + JSON.stringify(parameters, null, 4) + '\n\n\n\n\n\n\n');
//     async.parallel({
//         layoutTemplate: loadLayout,
//         templateValues: function getTemplateValues(callback) {
//             asyncObjectMap(parameters.configuration.slots, renderSlot, callback);
//         }
//     },

//     function (err, results) {
//         var template = handlebars.compile(results.layoutTemplate);
//         callback(err, template(results.templateValues));
//     });
// };