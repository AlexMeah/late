﻿define(['errorMessage'], function (errorMessage) {
    'use strict';

    return (function () {
        return {
            init: function (formElement) {
                var emailField = $('.email-field', formElement);

                var placeholder = emailField.attr('placeholder');

                formElement.on('submit', function (e) {
                    var email = emailField.val();

                    if (!email || email === placeholder) {
                        e.preventDefault();
                        _gaq.push(['_trackEvent', 'HomePage', 'Social Interactions', 'Email Signup - Invalid Email'], function () {
                            errorMessage.show('.email-deals-error-msg');
                        });
                    } else {
                        _gaq.push(['_trackEvent', 'HomePage', 'Social Interactions', 'Email Signup - Valid Email']);
                    }
                });
            }
        };
    })();

});
