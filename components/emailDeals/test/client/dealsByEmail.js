define(['proclaim', 'components/emailDeals/assets/scripts/dealsByEmail'], function (proclaim, dealsByEmail) {

    describe('deals by email', function () {
        var formElement;
        var submitButton;
        var alertShown;
        var defaultPrevented;

        $.fn.modal = function () {
            alertShown = true;
        };

        beforeEach(function () {
            defaultPrevented = false;
            alertShown = false;

            _gaq = [];

            submitButton = $('<input class="btn-secondary" type="submit" value="Get deals"></input>');
            formElement = $('<form action="http://www.laterooms.com/en/Subscribe.aspx" method="post" id="deals-by-email-form">' +
                '<h2><span>FREE</span> email deals</h2>' +
                '<input type="email" name="txtDByEmail" class="email-field" placeholder="Enter your email address">' +
                '</form>').append(submitButton);
            $('body').append(formElement);

            dealsByEmail.init(formElement);

            formElement.on('submit', function (e) {
                defaultPrevented = e.isDefaultPrevented();
                e.preventDefault();
                return false;
            });
        });

        afterEach(function () {
            $('.fixture').remove();
        });

        it('should display an error message when no email address has been entered', function () {
            submitButton.trigger('click');
            _gaq[1]();

            proclaim.equal(alertShown, true);
        });

        it('does not submit the form when no email address has been entered', function () {
            submitButton.trigger('click');

            proclaim.equal(defaultPrevented, true);
        });

        it('submits the form when an email address is entered', function () {
            $('.email-field', formElement).val('test@example.com');
            submitButton.trigger('click');

            proclaim.equal(alertShown, false);
        });

        it('should track button click in analytics for invalid email', function () {
            submitButton.trigger('click');
            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Social Interactions');
            proclaim.equal(gaqValue[3], 'Email Signup - Invalid Email');
        });

        it('should track button click in analytics for valid email', function () {
            $('input[name="txtDByEmail"]').val('test@test.com');
            submitButton.trigger('click');
            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Social Interactions');
            proclaim.equal(gaqValue[3], 'Email Signup - Valid Email');
        });
    });

});
