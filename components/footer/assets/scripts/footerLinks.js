define(function () {

    return function () {

        var region;

        $('ul.regions li').on(TLRG.touchEvent, function () {

            if ($(this).data('region') === region) {
                $('ul.subregions, ul.regions li').removeClass('active');
                $('ul.subregions, ul.regions li').removeClass('hideLine');
                region = '';
            } else {
                region = $(this).data('region');
                $('ul.subregions, ul.regions li').removeClass('active');
                $('ul.subregions, ul.regions li').removeClass('hideLine');
                $(this).addClass('active');
                $('ul.subregions[data-subregion=' + region + ']').addClass('active');
                $(this).next().addClass('hideLine');
            }
        });

        //Region titles
        $('ul.regions li a').on('click', function (e) {
            e.preventDefault();
        });

        $('ul.regions li').on('click', function () {
            var title = $(this).attr('title');
            _gaq.push(['_trackEvent', 'HomePage', 'Footer', title]);
        });

        //Region links
        $('ul.subregions a').on('click', function (e) {
            e.preventDefault();
            var _this = this;
            var title = $(_this).attr('title');
            _gaq.push(['_trackEvent', 'HomePage', 'Footer', title], function () {
                document.location = $(_this)[0].href;
            });
        });

        //Footer site menu
        $('ul.links li a').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var text = $this.html();
            var location = $this.attr('href');
            _gaq.push(['_trackEvent', 'HomePage', 'Footer', text], function () {
                document.location = location;
            });
        });
    };
});
