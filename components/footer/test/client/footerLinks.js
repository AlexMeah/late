define(['proclaim', 'components/footer/assets/scripts/footerLinks'], function (proclaim, footerLinks) {

    TLRG = {
        touchEvent: 'click'
    };

    describe('Footer', function () {

        beforeEach(function () {

            _gaq = [];

            var html =
                '<div class="fixture">' +
                '  <ul class="regions">' +
                '    <li title="Hotels in England" data-region="england"><a href="/en/r1_hotels-in-england.aspx">England</a></li>' +
                '    <li title="Hotels in Scotland" data-region="scotland"><a href="/en/r2_hotels-in-scotland.aspx">Scotland</a></li>' +
                '    <li title="Hotels in Wales" data-region="wales"><a href="/en/r3_hotels-in-wales.aspx">Wales</a></li>' +
                '    <li title="Hotels in Ireland" data-region="ireland"><a href="/en/r5_hotels-in-republic-of-ireland.aspx">Ireland</a></li>' +
                '  </ul>' +
                '  <ul class="subregions" data-subregion="england">' +
                '    <li>' +
                '      <a title="Hotels in London" href="k16295585_london-hotels.aspx">London Hotels</a>' +
                '   </li>' +
                '   <li>' +
                '     <a title="Hotels in Bath" href="k16279504_bath-hotels.aspx">Bath Hotels</a>' +
                '   </li>' +
                '  </ul>' +
                '  <ul class="subregions" data-subregion="scotland"></ul>' +
                '  <ul class="links">' +
                '    <li>' +
                '      <a rel="nofollow" href="http://laterooms.custhelp.com/app/home">Contact Us</a>' +
                '    </li>' +
                '  </ul>' +
                '</div>';
            $('body').append(html);

            footerLinks();
        });

        afterEach(function () {
            $('.fixture').remove();
        });

        it('should display subregion when footer link #1 is clicked', function () {
            $('ul.regions li:first-child').trigger('click');
            proclaim.equal($('ul.subregions[data-subregion="england"]').hasClass('active'), true);
        });

        it('should display subregion when footer link #2 is clicked', function () {
            $('ul.regions li:nth-child(2)').trigger('click');
            proclaim.equal($('ul.subregions[data-subregion="scotland"]').hasClass('active'), true);
        });

        it('should only display one subregion at a time', function () {
            $('ul.regions li:first-child').trigger('click');
            $('ul.regions li:nth-child(2)').trigger('click');
            proclaim.equal($('ul.subregions[data-subregion="england"]').hasClass('active'), false);
            proclaim.equal($('ul.subregions[data-subregion="scotland"]').hasClass('active'), true);
        });

        it('should highlight selected region', function () {
            $('ul.regions li:first-child').trigger('click');
            proclaim.equal($('ul.regions li:first-child').hasClass('active'), true);
        });

        it('should only highlight one selected region at a time', function () {
            $('ul.regions li:first-child').trigger('click');
            $('ul.regions li:last-child').trigger('click');
            proclaim.equal($('ul.regions li:first-child').hasClass('active'), false);
            proclaim.equal($('ul.regions li:last-child').hasClass('active'), true);
        });

        it('should hide highlighted seperator on either side of selected region', function () {
            $('ul.regions li:nth-child(2)').trigger('click');
            proclaim.equal($('ul.regions li:nth-child(2)').hasClass('active'), true);
            proclaim.equal($('ul.regions li.active').next().hasClass('hideLine'), true);
        });

        it('should hide one seperator for each selected region', function () {
            $('ul.regions li:nth-child(2)').trigger('click');
            $('ul.regions li:nth-child(3)').trigger('click');
            proclaim.equal($('ul.regions li:nth-child(2)').next().hasClass('hideLine'), false);
            proclaim.equal($('ul.regions li:nth-child(3)').next().hasClass('hideLine'), true);
        });

        it('should toggle selected region', function () {
            $('ul.regions li:nth-child(2)').trigger('click');
            $('ul.regions li:nth-child(2)').trigger('click');
            proclaim.equal($('ul.regions li:nth-child(2)').hasClass('active'), false);
        });

        it('should log top level region clicks in analytics', function () {
            $('ul.regions li:first-child').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Footer');
            proclaim.equal(gaqValue[3], 'Hotels in England');
        });

        it('should log sub level link clicks in analytics', function () {
            $('ul.subregions li:first-child a').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Footer');
            proclaim.equal(gaqValue[3], 'Hotels in London');
        });

        it('should log section link clicks in analytics', function () {
            $('ul.links li a').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Footer');
            proclaim.equal(gaqValue[3], 'Contact Us');
        });
    });
});
