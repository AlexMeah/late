define(['proclaim', 'components/partnerBrands/assets/scripts/partnerBrands'], function (proclaim, partnerBrands) {
    describe('Partner Brands', function () {

        beforeEach(function () {

            _gaq = [];

            var html =
                '<section class="partner-brands">' +
                '  <h2>Our partner brands</h2>' +
                '  <ul>' +
                '    <li class="partner-brand-devere">' +
                '      <a href="http://images.laterooms.com/en/hotel-offers/de-vere-hotels.html" title="De Vere"></a>' +
                '    </li>' +
                '  </ul>' +
                '</section>';
            $('body').append(html);

            partnerBrands();
        });

        afterEach(function () {
            $('.fixture').remove();
        });

        it('should log clicks in analytics', function () {
            $('.partner-brands ul li a').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Partner Brands');
            proclaim.equal(gaqValue[3], 'De Vere');
        });
    });
});
