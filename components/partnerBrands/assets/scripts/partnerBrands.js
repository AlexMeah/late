define(function () {

    return function () {
        $('.partner-brands a').on('click', function (e) {
            e.preventDefault();
            var _this = this;
            var title = $(_this).attr('title');
            _gaq.push(['_trackEvent', 'HomePage', 'Partner Brands', title], function () {
                document.location = $(_this)[0].href;
            });
        });
    };

});
