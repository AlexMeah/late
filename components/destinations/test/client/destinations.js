define(['proclaim', 'components/destinations/assets/scripts/destinations'], function (proclaim, destinations) {

    describe('destinations', function () {
        beforeEach(function () {

            _gaq = [];

            var html =
                '<article class="destination">' +
                '<a href="http://www.ltest.com" title="Test Title">' +
                '<img src="images/destinations/test.jpg">' +
                '</a>' +
                '</article>';

            $('body').append(html);

            destinations();

        });

        it('should log click in analytics', function () {

            $('.destination a').trigger('click');

            var gaqValue = _gaq[0];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'Popular Destinations');
            proclaim.equal(gaqValue[3], 'Test Title');
        });
    });

});
