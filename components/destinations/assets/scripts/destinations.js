define(function () {
    'use strict';

    return function () {

        $('.destination a').on('click', function (e) {
            e.preventDefault();
            var _this = this;

            var destinationsTitle = $(_this).attr('title');
            _gaq.push(['_trackEvent', 'HomePage', 'Popular Destinations', destinationsTitle],
                function () {
                    document.location = $(_this)[0].href;
                }
            );
        });
    };
});
