define(function () {
    var fieldMap = {
        Adults: 'rt-adult',
        Children: 'rt-child',
        Date: 'd',
        Nights: 'n'
    };

    var searchCookie = {
        load: function () {
            var cookie = $.cookie('search');

            if (!cookie) {
                return;
            }

            try {
                cookie = decodeURIComponent(cookie);
                cookie = $.parseJSON(cookie);
            } catch (e) {
                // Do nothing, cookie is invalid.
                return;
            }

            return cookie;
        },
        save: function (cookie) {
            cookie = JSON.stringify(cookie);
            cookie = encodeURIComponent(cookie);

            $.cookie('search', cookie);
        }
    };

    var rtCookie = {
        save: function (searchCookie) {
            $.cookie('rt', searchCookie.Adults + '-' + searchCookie.Children);
        }
    };

    var prePopulateSearchBar = function prePopulateSearchBar() {
        var cookie = searchCookie.load();
        if (!cookie) {
            // Nothing to set.
            return;
        }

        for (var field in fieldMap) {
            var value = cookie[field];
            if (!value) {
                continue;
            }

            var fieldName = fieldMap[field];

            $('[name=' + fieldName + ']').val(value);
        }

        $('[name=rt]').val(cookie.Adults + '-' + cookie.Children);
    };

    var bindSearchUpdate = function bindSearchUpdate() {
        var form = $('.search-form');
        var combinedField = form.find('[name=rt]');

        var adults = form.find('[name=rt-adult]');
        var children = form.find('[name=rt-child]');

        var updateRooms = function updateRooms() {
            var currentAdults = adults.val();
            var currentChildren = children.val();

            combinedField.val(currentAdults + '-' + currentChildren);
        };

        form.on('change', '[name=rt-adult], [name=rt-child]', updateRooms);

        var cookieSetters = [];

        var setCookieField = function (cookieFieldName, formField) {
            return function setField(cookie) {
                cookie[cookieFieldName] = formField.val();
            };
        };

        for (var fieldName in fieldMap) {
            var elementName = fieldMap[fieldName];

            var field = form.find('[name=' + elementName + ']');

            cookieSetters.push(setCookieField(fieldName, field));
        }

        var setSearchCookie = function setSearchCookie() {
            var cookie = {};

            for (var i = cookieSetters.length; i--;) {
                var setCookieField = cookieSetters[i];
                setCookieField(cookie);
            }

            searchCookie.save(cookie);
            rtCookie.save(cookie);
        };

        form.on('change', 'select, input', setSearchCookie);
    };

    return function searchBar() {
        prePopulateSearchBar();

        bindSearchUpdate();
    };
});
