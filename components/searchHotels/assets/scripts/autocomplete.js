﻿define(function () {
    'use strict';

    return function () {
        var searchBoxId,
            autocompleteId = '#autocomplete',
            autoCompleteHovered = false,
            currentAjaxRequest = null,
            searchForm;

        this.initialise = function (searchBox) {
            searchBoxId = searchBox;
            addSearchBoxEvents();
            searchForm = $(searchBox).parents('form.search-form');
        };

        this.stub = function stubAutoComplete() {
            var oldAjax = $.ajax;

            $.ajax = function (url) {
                if (!~url.indexOf('AutoComplete')) {
                    return oldAjax.apply($, arguments);
                }

                var response = {
                    Results: [{
                        Url: 'k16296355_manchester-hotels',
                        Count: 118,
                        Text: 'Manchester',
                        HotelId: 0
                    }, {
                        Url: 'k14678312_oslo-hotels',
                        Count: 11,
                        Text: 'Oslo',
                        HotelId: 0
                    }, {
                        Url: 'k12936832_berlin-hotels',
                        Count: 214,
                        Text: 'Berlin',
                        HotelId: 0
                    }, {
                        Url: 'k12835253_paris-hotels',
                        Count: 566,
                        Text: 'Paris',
                        HotelId: 0
                    }, {
                        Url: 'k16295585_london-hotels',
                        Count: 979,
                        Text: 'London',
                        HotelId: 0
                    }]
                };

                onRequestComplete(response);
            };
        };

        function addSearchBoxEvents() {
            $(searchBoxId).on('keyup', sendRequest);
            $(searchBoxId).on('blur', clearAutoCompleteIfNotHovered);
        }

        function clearAutoCompleteIfNotHovered() {
            if (!autoCompleteHovered) {
                destroyAutoComplete();
            }
        }

        function sendRequest() {
            if ($(searchBoxId).val().length > 2) {
                currentAjaxRequest = $.ajax(makeUrl(), {
                    success: onRequestComplete,
                    dataType: 'json',
                    method: 'POST'
                });
            } else {
                destroyAutoComplete();
            }
        }

        function makeUrl() {
            var searchTerm = $(searchBoxId).val();
            return '/en/AutoComplete/Index.mvc?id=' + formatSearchTerm(searchTerm);
        }

        function formatSearchTerm(searchTerm) {
            return encodeURI(searchTerm.toLowerCase());
        }

        function onRequestComplete(response) {
            destroyAutoComplete();
            if (response.Results && response.Results.length > 0) {
                createAutoComplete();
                injectList(response);
                attachClickEventToListItems();
            }
        }

        function injectList(response) {
            var optionsLimit = 8;

            var list = $('<ul class="search-autocomplete-options"></ul>');
            var len = response.Results.length < optionsLimit ?
                response.Results.length :
                optionsLimit;
            var i = 0;

            for (; i < len; i++) {

                list.append('<li data-url="' + response.Results[i].Url + '"><span class="location">' + response.Results[i].Text + '</span><span class="available-result-count">' + response.Results[i].Count + ' hotels</span></li>');
            }
            $(autocompleteId).append(list);
        }

        function createAutoComplete() {
            $(searchBoxId).after('<div id="' + autocompleteId.substring(1) + '"></div>');
            addAutoCompleteEvents();
        }

        function addAutoCompleteEvents() {
            $(autocompleteId).on('mouseover', function () {
                autoCompleteHovered = true;
            });
            $(autocompleteId).on('mouseout', function () {
                autoCompleteHovered = false;
            });
        }

        function destroyAutoComplete() {
            if (currentAjaxRequest) {
                currentAjaxRequest.abort();
            }

            if ($(autocompleteId) !== null) {
                $(autocompleteId).remove();
            }
        }

        function attachClickEventToListItems() {
            $(autocompleteId + ' li').on('click', function () {
                performClickEvent(this);
            });
        }

        function performClickEvent(ele) {
            var hotelLocation = $(ele).children('span.location').text();

            hotelLocation = checkForLocationOverride(hotelLocation);

            $(searchBoxId).val(hotelLocation);
            var searchUrl = '/en/' + $(ele).attr('data-url') + '.aspx';
            searchForm.attr('action', searchUrl);
            destroyAutoComplete();
        }

        function checkForLocationOverride(hotelLocation) {
            var locationOverride = {
                'Newcastle, Tyne and Wear, England': 'Newcastle',
                'Leeds, West Yorkshire, Yorkshire & The Dales, North England, England, UK, Europe': 'Leeds',
                'Belfast, County Antrim, Northern Ireland': 'Belfast',
                'Belfast City, County Antrim, Northern Ireland': 'Belfast',
                'Brighton, East Sussex, England': 'Brighton',
                'Scotland, UK, Europe': 'Scotland',
                'Leicester, Leicestershire, England': 'Leicester',
                'Leicester, Leicestershire, East Midlands, Central England, England, UK, Europe': 'Leicester',
                'Cambridge, Cambridgeshire, England': 'Cambridge',
                'Durham, County Durham, England': 'Durham',
                'Lincoln, Lincolnshire, England': 'Lincoln',
                'Plymouth, Devon, England': 'Plymouth',
                'Milton Keynes, Buckinghamshire, England': 'Milton Keynes',
                'Milton Keynes, Buckinghamshire, South East England, England, UK, Europe': 'Milton Keynes',
                'Exeter, Devon, England': 'Exeter',
                'Cheshire, North West England, North England, England, UK, Europe': 'Cheshire',
                'Northampton, Northamptonshire, England': 'Northampton',
                'Wales, UK, Europe': 'Wales',
                'Watford, Greater London, England': 'Watford',
                'Essex, South East England, England': 'Essex',
                'Bradford, West Yorkshire, Yorkshire & The Dales, North England, England, UK, Europe': 'Bradford',
                'Bolton, East Lothian, Scotland': 'Bolton',
                'Newport, South Wales, Wales': 'Newport',
                'Luton, Bedfordshire, England': 'Luton',
                'Worcester, West Midlands, England': 'Worcester',
                'Darlington, County Durham, North East England, North England, England, UK, Europe': 'Darlington',
                'Middlesbrough, North East England, England': 'Middlesbrough',
                'Middlesbrough, North Yorkshire, Yorkshire & The Dales, North England, England, UK, Europe': 'Middlesbrough',
                'Norfolk, East Anglia, England, UK, Europe': 'Norfolk',
                'Chesterfield, Derbyshire, England': 'Chesterfield',
                'Lancaster, Lancashire, England': 'Lancaster',
                'Greenwich, Greater London, England': 'Greenwich',
                'Hampshire, Central and South England, England, UK, Europe': 'Hampshire',
                'Mayfair, Greater London, England': 'Mayfair'
            };

            if (hotelLocation in locationOverride) {
                hotelLocation = locationOverride[hotelLocation];
            }

            return hotelLocation;
        }
    };
});
