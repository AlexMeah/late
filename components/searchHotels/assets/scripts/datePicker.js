define(['jquerydatepicker'], function () {
    'use strict';

    var today = setTodaysDate();
    var endDate = aYearHence(today);

    var dom = {
        hiddenInputField: '.input-hidden',
        visibleDateSelector: '.date-select',
        bootstrapDatePicker: '.datepicker',
        domElementToAppendDatePickerTo: '.datepicker-placeholder',
        datePickerToggler: '.cal-icon'
    };

    Date.prototype.addDays = function (days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };

    return {
        init: function () {
            $(dom.hiddenInputField).datepicker({
                autoclose: true,
                format: 'yyyymmdd',
                startDate: today,
                endDate: endDate
            });

            setDate();

            $(dom.hiddenInputField).on('changeDate', function () {
                $(dom.visibleDateSelector).val($(dom.hiddenInputField).val());
            });

            $(dom.hiddenInputField).on('show', function () {
                $('.datepicker-switch').click(function (e) {
                    e.stopPropagation();
                });
                $(dom.bootstrapDatePicker).appendTo(dom.domElementToAppendDatePickerTo);
            });

            $(dom.visibleDateSelector).on('change', setDate);
            $(dom.datePickerToggler).on('click', toggleDatePicker);
        }
    };

    function padZeros(number) {
        number = number.toString();
        while (number.length < 2) {
            number = '0' + number;
        }

        return number;
    }

    function formatDate(date) {
        var output = [];

        output.push(date.getFullYear());
        output.push('-');
        output.push(padZeros(date.getMonth() + 1));
        output.push('-');
        output.push(padZeros(date.getDate()));

        return output.join('');
    }

    function setDate() {
        var daysAHead = $(dom.visibleDateSelector + ' option:selected').index();

        var selectedDate = new Date().addDays(daysAHead);

        var formattedDate = formatDate(selectedDate);

        $(dom.hiddenInputField).datepicker('update', formattedDate);
    }

    function toggleDatePicker() {
        if ($(dom.bootstrapDatePicker).length >= 1) {
            $(dom.hiddenInputField).datepicker('hide');
        } else {
            $(dom.hiddenInputField).datepicker('show');
        }
    }

    function setTodaysDate() {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        return today;
    }

    function aYearHence(today) {
        var inAYear = new Date();
        inAYear.setDate(today.getDate() + 364);
        inAYear.setHours(0, 0, 0, 0);

        return inAYear;
    }

});
