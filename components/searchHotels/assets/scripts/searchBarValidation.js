define(['errorMessage'], function (errorMessage) {
    var init = function initSearchBarValidation() {
        var form = $('.search-form');

        var criteria = form.find('#txtSearch');
        var placeholder = criteria.attr('placeholder');

        form.on('submit', function onSearchSubmission(e) {
            var currentCriteria = $.trim(criteria.val());

            if (currentCriteria && currentCriteria !== placeholder) {
                return;
            }

            e.preventDefault();

            errorMessage.show('.validation-error-msg');
        });
    };

    return init;
});
