define(function () {
    'use strict';

    Date.prototype.addDays = function (days)
    {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };

    var dateToYMD = function (date) {
        var d = date.getDate();
        var m = date.getMonth() + 1;
        var y = date.getFullYear();
        return '' + y + '' + (m <= 9 ? '0' + m : m) + '' + (d <= 9 ? '0' + d : d);
    };

    var daysOfTheWeek = [
        'Sun',
        'Mon',
        'Tue',
        'Wed',
        'Thu',
        'Fri',
        'Sat'
    ];

    var monthsOfYear = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ];

    var formatReadableDate = function (date) {
        var str = '';

        str += daysOfTheWeek[date.getDay()] + ' ';
        str += date.getDate() + ' ';
        str += monthsOfYear[date.getMonth()] + ' ';
        str += date.getFullYear();

        return str;
    };

    return {
        init: function () {
            var dt = new Date();

            var options = '';

            for (var i = 0; i < 365; i++) {
                if (i === 0) {
                    options += '<option value="' + dateToYMD(dt.addDays(i)) + '" selected="selected">' + formatReadableDate(dt.addDays(i)) + '</option>';
                }
                else {
                    options += '<option value="' + dateToYMD(dt.addDays(i)) + '">' + formatReadableDate(dt.addDays(i)) + '</option>';
                }
            }

            $('.date-select').html(options);
        }
    };

});
