define(['proclaim', 'components/searchHotels/assets/scripts/autocomplete'], function (proclaim, Autocomplete) {
    'use strict';

    describe('autocomplete', function () {
        beforeEach(function () {
            $('body').append(
                '<div class="fixture">' +
                '<form action="/Hotels.aspx" class="search-form">' +
                '<div class="search-container">' +
                '<input type="text" id="txtSearch" name="k" class="search-keyword" placeholder="Your location" value="" />' +
                '</div>' +
                '</form>' +
                '</div>');
        });

        afterEach(function () {
            $('.fixture').remove();
            $.mockjaxClear();
        });

        it('should make a request when the text box is updated', function (done) {
            var autoComplete = new Autocomplete();

            // TODO: Multi-language support.
            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                responseText: JSON.stringify(fakeResponse),
                response: function () {
                    done();
                }
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('mal');
            $('#txtSearch').trigger('keyup');
        });

        it('should encode the search term', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=ma%20',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse),
                response: function () {
                    done();
                }
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('ma ');
            $('#txtSearch').trigger('keyup');
        });

        it('should lowercase the search term', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse),
                response: function () {
                    done();
                }
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');
        });

        it('should not trigger a search if there are less than three characters', function (done) {
            var autoComplete = new Autocomplete(),
                ajaxCalled = false;

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse),
                response: function () {
                    ajaxCalled = true;
                }
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('ma');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                proclaim.isFalse(ajaxCalled);
                done();
            }, 5);
        });

        it('results should be displayed to the user in a list', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse)
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                var autocomplete = $('#autocomplete');

                var itemCount = autocomplete.find('li').length;
                proclaim.equal(itemCount, 2);

                var locations = $('#autocomplete').find('span.location');
                locations = $.map(locations, function (location) {
                    return $(location).html();
                });

                var destinations = ['Malta', 'Malaysia'];

                proclaim.deepEqual(locations, destinations);

                done();
            }, 5);
        });

        it('should not display the autocomplete box if there are no results', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify({
                    Results: []
                })
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                var length = !('#autocomplete').length;
                proclaim.equal(length, 0);
                done();
            }, 5);
        });

        it('should only display the first eight results', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify({
                    Results: [{
                        Text: 'one'
                    }, {
                        Text: 'two'
                    }, {
                        Text: 'three'
                    }, {
                        Text: 'four'
                    }, {
                        Text: 'five'
                    }, {
                        Text: 'six'
                    }, {
                        Text: 'seven'
                    }, {
                        Text: 'eight'
                    }]
                })
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                var length = $('#autocomplete li').length;
                proclaim.equal(length, 8);
                done();
            }, 5);
        });

        it('should update the textbox when an option is selected', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse)
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                $('#autocomplete li:eq(1)').click();

                var value = $('#txtSearch').val();
                proclaim.equal(value, 'Malaysia');
                var length = $('#autocomplete').length;
                proclaim.equal(length, 0);

                done();
            }, 5);
        });

        it('should update the containing forms action', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse)
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');

            var expectedAction = '/en/' + fakeResponse.Results[1].Url + '.aspx';

            setTimeout(function () {
                $('#autocomplete li:eq(1)').click();

                var action = $('form.search-form').attr('action');
                proclaim.equal(action, expectedAction);

                done();
            }, 5);
        });

        it('should update autocomplete each time the text box is updated', function (done) {
            var autoComplete = new Autocomplete(),
                ajaxCalledNTimes = 0;

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=ma*',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse),
                response: function () {
                    ajaxCalledNTimes++;
                }
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                $('#txtSearch').val('MALA');
                $('#txtSearch').trigger('keyup');
                setTimeout(function () {
                    proclaim.equal(ajaxCalledNTimes, 2);

                    done();
                }, 15);
            }, 5);
        });

        it('should remove autocomplete when less than three chars', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse)
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');
            $('#txtSearch').val('MA');
            $('#txtSearch').trigger('keyup');

            setTimeout(function () {
                var length = $('#autocomplete').length;
                proclaim.equal(length, 0);
                done();
            }, 5);
        });

        it('should remove autocomplete when text box is blurred', function (done) {
            var autoComplete = new Autocomplete();

            $.mockjax({
                url: '/en/AutoComplete/Index.mvc?id=mal',
                responseTime: 1,
                logging: false,
                dataType: 'json',
                responseText: JSON.stringify(fakeResponse)
            });

            autoComplete.initialise('#txtSearch');

            $('#txtSearch').val('MAL');
            $('#txtSearch').trigger('keyup');
            $('#txtSearch').trigger('blur');

            setTimeout(function () {
                var length = $('#autocomplete').length;
                proclaim.equal(length, 0);

                done();
            }, 5);
        });
    });

    var fakeResponse = {
        SearchTerm: 'ma',
        Results: [{
            Url: 'r50_hotels-in-malta',
            Count: 112,
            Text: 'Malta',
            HotelId: 0
        }, {
            Url: 'r49_hotels-in-malaysia',
            Count: 557,
            Text: 'Malaysia',
            HotelId: 0
        }]
    };

});
