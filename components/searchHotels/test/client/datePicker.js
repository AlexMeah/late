define(['proclaim', 'components/searchHotels/assets/scripts/datePicker', 'jquerydatepicker'], function (proclaim, datePicker) {
    'use strict';

    describe('date picker', function () {
        var today = new Date();
        today.setHours(0,0,0,0);

        var aYearHence = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);
        aYearHence.setDate(today.getDate() + 364);

        var dateToYMD = function (date) {
            var d = date.getDate();
            var m = date.getMonth() + 1;
            var y = date.getFullYear();
            return '' + y + '' + (m <= 9 ? '0' + m : m) + '' + (d <= 9 ? '0' + d : d);
        };

        function getTomorrow () {
            var tomorrow = new Date();
            var today = new Date();
            tomorrow.setDate(today.getDate()+1);

            return tomorrow;
        }

        function setUpDateXDaysInFuture (num) {
            var today = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(today.getDate() + num);

            return dateToYMD(tomorrow);
        }

        function setUpSelectOptions () {
            setUpOption(setUpDateXDaysInFuture(0));
            setUpOption(setUpDateXDaysInFuture(1));
            setUpOption(setUpDateXDaysInFuture(2));
        }

        function setUpOption (value) {
            $('select').append('<option value="'+ value +'"></option>');
        }

        beforeEach(function () {
            $('body').append('<div class="fixture">' +
                    '<input type="hidden" class="input-hidden" value="' + dateToYMD(today) + '"/>' +
                    '<select class="date-select"></select>' +
                    '<div class="datepicker-placeholder"></div>' +
                    '<button class="cal-icon"></button>' +
                '</div>');
            setUpSelectOptions();

            datePicker.init();
        });

        afterEach(function () {
            $('.fixture').remove();
            $('.datepicker').remove();
        });

        it('config should have start/end date, autoclose on selection, and correct date format specified', function (done) {

            var originalImplementation = jQuery.fn.datepicker;

            jQuery.fn.datepicker = function (options) {
                proclaim.equal(options.startDate.toString(), today.toString());
                proclaim.equal(options.endDate.toString(), aYearHence.toString());
                proclaim.equal(options.format.toString(), 'yyyymmdd');
                proclaim.equal(options.autoclose.toString(), 'true');
                jQuery.fn.datepicker = originalImplementation;
                done();
            };

            datePicker.init();
        });

        it('should display picker when cal icon is clicked', function (done) {
            $('.cal-icon').click();

            setTimeout(function () {
                var length = $('.datepicker').length;
                proclaim.equal(length, 1);

                done();
            }, 10);
        });

        it('should hide picker when cal icon is clicked and calendar is visible', function (done) {
            $('.cal-icon').click();
            $('.cal-icon').click();
            setTimeout(function () {
                var length = $('.datepicker').length;
                proclaim.equal(length, 0);

                done();
            }, 10);
        });

        it('should update date field when date is selected from picker', function () {
            $('.input-hidden').datepicker('setDate', getTomorrow());
            var value = $('.input-hidden').val();
            proclaim.equal(value, dateToYMD(getTomorrow()));
        });

        it('should update select box with selected date from picker', function () {
            $('.input-hidden').datepicker('setDate', getTomorrow());
            var value = $('select').val();
            proclaim.equal(value, dateToYMD(getTomorrow()));
        });

        it('should update the hidden field when selecting a date from the drop-down', function () {
            $('select').val(setUpDateXDaysInFuture(2));
            $('select').trigger('change');
            var value = $('.input-hidden').val();
            proclaim.equal(value, setUpDateXDaysInFuture(2));
        });

        it('should append to datepicker-placeholder div', function (done) {
            $('.cal-icon').click();

            setTimeout(function () {
                var length = $('.datepicker-placeholder .datepicker').length;
                proclaim.equal(length, 1);

                done();
            }, 10);
        });

        it('should read start date from hidden field', function () {
            var selectedDate = dateToYMD($('.input-hidden').datepicker('getDate'));
            var value = $('.input-hidden').val();
            proclaim.equal(value, selectedDate);
        });
    });
});
