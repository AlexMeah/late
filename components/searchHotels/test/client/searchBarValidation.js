define(['proclaim', 'components/searchHotels/assets/scripts/searchBarValidation'], function (proclaim, searchBarValidation) {
    describe('Search Bar Validation', function () {
        var context = $('<form class="search-form">' +
                      '<input type="text" id="txtSearch" name="k" />' +
                      '</form>');

        var oldModal = $.fn.modal;

        beforeEach(function () {
            $('body').append(context);

            searchBarValidation();

            // Don't use the actual modal
            $.fn.modal = function () {};
        });

        afterEach(function () {
            $(context).remove();

            $.fn.modal = oldModal;
        });

        it('should not allow submission without a search term.', function (done) {
            var form = context;
            form.on('submit', function (e) {
                proclaim.equal(e.isDefaultPrevented(), true);

                done();
            });

            form.trigger('submit');
        });

        // Ed: Personal vendetta.
        it('should not allow submission with an empty search term.', function (done) {
            var form = context;
            form.find('input').val('    ');

            form.on('submit', function (e) {
                proclaim.equal(e.isDefaultPrevented(), true);

                done();
            });

            form.trigger('submit');
        });

        it('should show a validation message when submission fails.', function (done) {
            var form = context;

            $.fn.modal = function () {
                done();
            };

            form.trigger('submit');
        });

        it('should allow submission with a search term.', function (done) {
            var form = context;
            form.find('#txtSearch').val('Some criteria');

            form.on('submit', function (e) {
                proclaim.equal(e.isDefaultPrevented(), false);

                done();

                e.preventDefault();
            });

            form.trigger('submit');
        });
    });
});
