define(['proclaim', 'components/searchHotels/assets/scripts/searchBar', 'jquerycookie'], function (proclaim, searchBar) {
    describe('Search Bar', function () {
        var context;

        var searchCookie = 'search';

        beforeEach(function () {
            // Need to replace the hard-coded HTML with something a bit more resillient.
            context = $('<form class="search-form">' +
                    '<input type="text" id="txtSearch" name="k" placeholder="Destination" />' +
                    '<select class="date-select" name="d">' +
                    '    <option value="20140329"></option>' +
                    '    <option value="20140330"></option>' +
                    '    <option value="20140331"></option>' +
                    '    <option value="20140401"></option>' +
                    '    <option value="20140402"></option>' +
                    '    <option value="20140403"></option>' +
                    '</select>' +
                    '<select name="n">' +
                    '    <option value="1">1 Night</option>' +
                    '    <option value="2">2 Nights</option>' +
                    '    <option value="3">3 Nights</option>' +
                    '    <option value="4">4 Nights</option>' +
                    '    <option value="5">5 Nights</option>' +
                    '    <option value="6">6 Nights</option>' +
                    '    <option value="7">7 Nights</option>' +
                    '    <option value="8">8 Nights</option>' +
                    '    <option value="9">9 Nights</option>' +
                    '    <option value="10">10 Nights</option>' +
                    '    <option value="11">11 Nights</option>' +
                    '    <option value="12">12 Nights</option>' +
                    '    <option value="13">13 Nights</option>' +
                    '    <option value="14">14 Nights</option>' +
                    '</select>' +
                    '<input type="hidden" name="rt" value="2-0" />' +
                    '<select name="rt-adult">' +
                    '    <option value="1">1 Adult</option>' +
                    '    <option value="2" selected="selected">2 Adults</option>' +
                    '    <option value="3">3 Adults</option>' +
                    '    <option value="4">4 Adults</option>' +
                    '    <option value="5">5 Adults</option>' +
                    '    <option value="6">6 Adults</option>' +
                    '    <option value="7">7 Adults</option>' +
                    '    <option value="8">8 Adults</option>' +
                    '</select>' +
                    '<select name="rt-child">' +
                    '    <option value="0">0 Children</option>' +
                    '    <option value="1">1 Child</option>' +
                    '    <option value="2">2 Children</option>' +
                    '    <option value="3">3 Children</option>' +
                    '</select>' +
                    '</form>');

            $('body').append(context);

            console.log();
            // Clear the saved search cookie.
            $.cookie(searchCookie, '');
        });

        afterEach(function () {
            $(context).remove();
        });

        function getFormContent() {
            var array = context.serializeArray();

            var result = {};

            $.each(array, function () {
                result[this.name] = this.value || '';
            });

            return result;
        }

        function setCookie(cookie) {
            cookie = JSON.stringify(cookie);
            cookie = encodeURIComponent(cookie);

            $.cookie(searchCookie, cookie);
        }

        function setFormContent(criteria) {
            for (var name in criteria) {
                var value = criteria[name];

                $('[name=' + name + ']').val(value).trigger('change');
            }
        }

        function getCookie() {
            var cookie = $.cookie(searchCookie);

            cookie = decodeURIComponent(cookie);

            return $.parseJSON(cookie);
        }

        describe('combines "rt-adult" and "rt-child" into "rt"', function () {
            var setPart = function setPart(fieldName, part) {
                if (part) {
                    context.find('[name=' + fieldName + ']').val(part).trigger('change');
                }
            };

            var setRooms = function setRooms(config) {
                setPart('rt-adult', config.adults);
                setPart('rt-child', config.children);
            };

            it('should be set to 2-0 by default.', function () {
                searchBar();

                var result = getFormContent();

                proclaim.equal(result.rt, '2-0');
            });

            it('should correctly reflect the rt-adult component.', function () {
                searchBar();

                setRooms({
                    adults: 4,
                    children: 2
                });

                var result = getFormContent();

                proclaim.equal(result.rt, '4-2');
            });
        });

        describe('keeps state between requests', function () {
            it('should have defaults selected if no cookie is present.', function () {
                searchBar();

                var result = getFormContent();

                proclaim.deepEqual(result, {
                    k: '',
                    d: '20140329',
                    n: '1',
                    'rt-adult': '2',
                    'rt-child': '0',
                    rt: '2-0'
                });
            });

            it('should retain previous search criteria.', function () {
                var previousSearch = {
                    Adults: 3,
                    Children: 1,
                    Date: '20140401',
                    Nights: 3
                };

                setCookie(previousSearch);

                searchBar();

                var result = getFormContent();

                proclaim.deepEqual(result, {
                    k: '',
                    d: '20140401',
                    n: '3',
                    'rt-adult': '3',
                    'rt-child': '1',
                    rt: '3-1'
                });
            });

            it('should not set the destination.', function () {
                var previousSearch = {
                    Destination: 'Somewhere in Mexico, at this rate.',
                    Adults: 3,
                    Children: 1,
                    Date: '20140401',
                    Nights: 3
                };

                setCookie(previousSearch);

                searchBar();

                var result = getFormContent();

                proclaim.deepEqual(result, {
                    k: '',
                    d: '20140401',
                    n: '3',
                    'rt-adult': '3',
                    'rt-child': '1',
                    rt: '3-1'
                });
            });

            it('should set the cookie when the criteria are changed.', function () {
                searchBar();

                var searchCriteria = {
                    k: '',
                    d: '20140402',
                    n: '2',
                    'rt-adult': '4',
                    'rt-child': '2',
                    rt: '4-2'
                };

                setFormContent(searchCriteria);

                var cookie = getCookie();

                proclaim.deepEqual(cookie, {
                    Adults: '4',
                    Children: '2',
                    Date: '20140402',
                    Nights: '2'
                });
            });

            it('should set the rt cookie when the criteria are changed.', function () {
                searchBar();

                var searchCriteria = {
                    k: '',
                    d: '20140402',
                    n: '2',
                    'rt-adult': '4',
                    'rt-child': '2',
                    rt: '4-2'
                };

                setFormContent(searchCriteria);

                var cookie = $.cookie('rt');

                proclaim.deepEqual(cookie, '4-2');
            });
        });
    });

});
