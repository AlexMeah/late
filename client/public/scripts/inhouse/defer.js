define('defer', function () {
    function defer (functionToExecuteOrScriptToLoad) {
        var functionToExecute = functionToExecuteOrScriptToLoad;

        if (typeof functionToExecuteOrScriptToLoad !== 'function') {
            functionToExecute = function () {
                var element = document.createElement( 'script' );
                element.src = functionToExecuteOrScriptToLoad;
                document.body.appendChild(element);
            };
        }

        setUpDeferredCall(functionToExecute);
    }

    function setUpDeferredCall (call) {
        if (window.onLoadTriggered) {
            return call();
        }

        if (window.addEventListener) {
            window.addEventListener('load', call, false);
        } else if (window.attachEvent) {
            window.attachEvent('onload', call);
        } else {
            var existingOnLoad = window.onload || function () {};
            window.onload = function () {
                call();
                existingOnLoad();
            };
        }
    }

    return defer;
});
