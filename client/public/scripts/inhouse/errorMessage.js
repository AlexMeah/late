define('errorMessage', ['jquerymodal'], function () {

    return {
        show: function showErrorMessage(selector) {
            $(selector).modal({
                overlayClose: true
            });
        }
    };

});
