require.config({
    paths: {
        jquerycookie: '../thirdparty/jquerycookie/jquery.cookie',
        jquerydatepicker: '../thirdparty/bootstrap-datepicker/js/bootstrap-datepicker',
        jquerymodal: '../thirdparty/simple-modal/js/jquery.simplemodal',
        json2: '../thirdparty/json2/json2',
        components: '../..'
    }
});

require([
    'components/searchHotels/assets/scripts/searchBar',
    'components/searchHotels/assets/scripts/searchBarValidation',
    'components/userNav/assets/scripts/currencyList',
    'components/searchHotels/assets/scripts/autocomplete',
    'components/cookieMessage/assets/scripts/cookieMessage',
    'components/searchHotels/assets/scripts/datePicker',
    'components/searchHotels/assets/scripts/datePickerSelect',
    'components/emailDeals/assets/scripts/dealsByEmail',
    'components/destinations/assets/scripts/destinations',
    'components/footer/assets/scripts/footerLinks',
    'components/mainNav/assets/scripts/mainNavigation',
    'components/partnerBrands/assets/scripts/partnerBrands',
    'components/userNav/assets/scripts/personalisation',
    'components/socialMedia/assets/scripts/socialMedia',
    'components/marketingPanel/assets/scripts/whatsNew',
    'defer',
    'homepageAnalytics',
    'json2'
],function (searchBar, searchBarValidation, currencyList, AutoComplete, cookieMessage, datePicker, datePickerSelect, dealsByEmail, destinations, footerLinks, mainNavigation, partnerBrands, personalisation, socialMedia, whatsNew, defer, homepageAnalytics) {
    searchBar();
    datePickerSelect.init();
    cookieMessage($.cookie);

    if ('ontouchstart' in document) {
        $('body').removeClass('no-touch');
        TLRG.touchEvent = 'touchstart';
    }

    searchBarValidation();

    var ac = new AutoComplete();
    ac.initialise('#txtSearch');
    // This sucks, but it's one way to put autocomplete intio
    // "test" mode while we don't have services backing it.
    if (/localhost/.test(window.location.origin)) {
        ac.stub();
    }

    mainNavigation();
    datePicker.init();

    defer(function deferExecution() {
        currencyList.init();
        footerLinks();
        dealsByEmail.init($('#deals-by-email-form'));
        socialMedia();
        whatsNew();
        destinations();
        partnerBrands();
        personalisation.init($.cookie, window);
        homepageAnalytics();
    });

    defer('http://images.laterooms.com/live-promotion/new-homepage-panels/marketingPanels.js');
});
