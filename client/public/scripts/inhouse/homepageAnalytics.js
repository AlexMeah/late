define('homepageAnalytics', function () {
    return function () {
        _gaq.push(['_setCustomVar', 1, 'HomePageMVT', 'NewVariant', 1]);
        _gaq.push(['_trackEvent', 'HomePage', 'MVT', '', null, true]);
    };
});
