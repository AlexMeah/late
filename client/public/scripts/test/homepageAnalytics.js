define(['proclaim', 'homepageAnalytics'], function(proclaim, homepageAnalytics){
    describe('homepageAnalytics', function () {

        beforeEach(function () {
            _gaq = [];

            homepageAnalytics();
        });

        it('should track page view on page load', function () {
            proclaim.equal(_gaq.length, 2);
            gaqValue = _gaq[0];
            proclaim.equal(gaqValue[0], '_setCustomVar');
            proclaim.equal(gaqValue[1], 1);
            proclaim.equal(gaqValue[2], 'HomePageMVT');
            proclaim.equal(gaqValue[3], 'NewVariant');
            proclaim.equal(gaqValue[4], 1);


            var gaqValue = _gaq[1];

            proclaim.equal(gaqValue[0], '_trackEvent');
            proclaim.equal(gaqValue[1], 'HomePage');
            proclaim.equal(gaqValue[2], 'MVT');
            proclaim.equal(gaqValue[3], '');
            proclaim.equal(gaqValue[4], null);
            proclaim.equal(gaqValue[5], true);
        });
    });
});
