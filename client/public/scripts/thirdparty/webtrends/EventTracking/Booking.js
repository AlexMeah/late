﻿var webtrendsEventTracking = {
    onTitleChange: function(element) {
        var guestElement;
        if (element && element.id === "roomtitle0") {
            guestElement = true;
        }

        var dcsuri = !guestElement ? '/bookingform/title' : '/bookingform/title_room';
        var title = !guestElement ? 'Title' : 'Title - guest';
        var scenarioName = !guestElement ? 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest' : 'OnlineFullBookingGuest';
        var scenarioNumber = !guestElement ? '1;1;1;1' : '7';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onFirstNameChange: function(element) {
        var guestElement;
        if (element.id === "firstname0") {
            guestElement = true;
        }

        var dcsuri = !guestElement ? '/bookingform/firstname' : '/bookingform/firstname_room';
        var title = !guestElement ? 'First Name' : "First Name - guest";
        var scenarioName = !guestElement ? 'OnlineFullBookingBooker;OnlineFullBookingGuest' : 'OnlineFullBookingGuest';
        var scenarioNumber = !guestElement ? '2;2' : '8';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onSurnameChange: function(element) {
        var guestElement;
        if (element.id === "lastname0") {
            guestElement = true;
        }

        var dcsuri = !guestElement ? '/bookingform/surname' : '/bookingform/surname_room';
        var title = !guestElement ? 'Surname' : 'Surname - guest';
        var scenarioName = !guestElement ? 'OnlineFullBookingBooker;OnlineFullBookingGuest' : 'OnlineFullBookingGuest';
        var scenarioNumber = !guestElement ? '3;3' : '9';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onEmailAddressChange: function(element) {
        var guestElement;
        if (element.id === "guest-email-0") {
            guestElement = true;
        }

        var dcsuri = !guestElement ? '/bookingform/emailaddress' : '/bookingform/emailaddress_room';
        var title = !guestElement ? 'Email Address' : 'Email Address - guest';
        var scenarioName = !guestElement ? 'OnlineFullBookingBooker;OnlineFullBookingGuest' : 'OnlineFullBookingGuest';
        var scenarioNumber = !guestElement ? '4;4' : '11';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onConfirmEmailAddressChange: function(element) {
        var guestElement;
        if (element.id === "guest-email-confirm-0") {
            guestElement = true;
        }
        
        var dcsuri = !guestElement ? '/bookingform/confirmemail' : '/bookingform/confirmemail_room';
        var title = !guestElement ? 'Confirm Email Address' : 'Confirm Email Address - guest';
        var scenarioName = !guestElement ? 'OnlineFullBookingBooker;OnlineFullBookingGuest' : 'OnlineFullBookingGuest';
        var scenarioNumber = !guestElement ? '5;5' : '12';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onSendConfirmEmailChange: function() {
        var dcsuri = '/bookingform/sendemail_room';
        var title = 'Send Guest Confirmation Email';
        var scenarioName = 'OnlineFullBookingGuest';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', '10');
    },

    onTelephoneChange: function() {
        var dcsuri = '/bookingform/telephone';
        var title = 'Telephone Number';
        var scenarioName = 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest';
        var scenarioNumber = '2;6;2;6';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onCountryChange: function() {
        var dcsuri = '/bookingform/country';
        var title = 'Country';
        var scenarioName = 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest';
        var scenarioNumber = '3;7;4;13';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onPostcodeChange: function() {
        var dcsuri = '/bookingform/postcode';
        var title = 'Postcode';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '8;14';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onAddressChange: function() {
        var dcsuri = '/bookingform/Address1';
        var title = 'Address1';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '9;15';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onTownChange: function() {
        var dcsuri = '/bookingform/town';
        var title = 'Town';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '10;16';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onCardTypeChange: function() {
        var dcsuri = '/bookingform/cardtype';
        var title = 'Card Type';
        var scenarioName = 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest';
        var scenarioNumber = '4;11;5;17';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onCardNumberChange: function() {
        var dcsuri = '/bookingform/cardnumber';
        var title = 'Card Number';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '12;18';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onNameOnCardChange: function() {
        var dcsuri = '/bookingform/nameoncard';
        var title = 'Name on Card';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '13;19';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onStartDateMMChange: function() {
        var dcsuri = '/bookingform/startdatemm';
        var title = 'Start Date MM';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '14;20';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onStartDateYYChange: function() {
        var dcsuri = '/bookingform/startdateyy';
        var title = 'Start Date YY';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '15;21';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onExpiryDateMMChange: function() {
        var dcsuri = '/bookingform/expirymm';
        var title = 'Expiry Date MM';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '16;22';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onExpiryDateYYChange: function() {
        var dcsuri = '/bookingform/expiryyy';
        var title = 'Expiry Date YY';
        var scenarioName = 'OnlineFullBookingBooker;OnlineFullBookingGuest';
        var scenarioNumber = '17;23';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onSecurityCodeChange: function() {
        var dcsuri = '/bookingform/securitycode';
        var title = 'Security Code';
        var scenarioName = 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest';
        var scenarioNumber = '5;18;6;24';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onConfirmTcsChange: function() {
        var dcsuri = '/bookingform/confirmtcs';
        var title = 'Confirm Terms and Conditions';
        var scenarioName = 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest';
        var scenarioNumber = '6;19;7;25';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onSubmit: function() {
        try {
            var dcsuri = '/bookingform/submit';
            var title = 'Submit Booking';
            var scenarioName = 'OnlineBookingBooker;OnlineFullBookingBooker;OnlineBookingGuest;OnlineFullBookingGuest';
            var scenarioNumber = '7;20;8;26';

            dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
        }
        catch (e) {
            // suppress exception as we don't want to block submitting the booking
        }
        finally {
            return true;
        }
    }
};






window.addEvent('domready', function() {
    var title = $('title');
    if (title) {
        title.addEvent('change', function() { webtrendsEventTracking.onTitleChange(title); });
    }

    var firstname = $('firstname');
    if (firstname) {
        firstname.addEvent('change', function() { webtrendsEventTracking.onFirstNameChange(firstname); });
    }

    var lastname = $('lastname');
    if (lastname) {
        lastname.addEvent('change', function() { webtrendsEventTracking.onSurnameChange(lastname); });
    }

    var email = $('email');
    if (email) {
        email.addEvent('change', function() { webtrendsEventTracking.onEmailAddressChange(email); });
    }

    var confirmemail = $('confirmemail');
    if (confirmemail) {
        confirmemail.addEvent('change', function() { webtrendsEventTracking.onConfirmEmailAddressChange(confirmemail); });
    }

    var telephone = $('telephone');
    if (telephone) {
        telephone.addEvent('change', function() { webtrendsEventTracking.onTelephoneChange(telephone); });
    }

    var country = $('mPay-billingCountry');
    if (country) {
        country.addEvent('change', function() { webtrendsEventTracking.onCountryChange(country); });
    }

    var postcode = $('mPay-billingPostcode');
    if (postcode) {
        postcode.addEvent('change', function() { webtrendsEventTracking.onPostcodeChange(postcode); });
    }

    var address1 = $('mPay-billingAddress');
    if (address1) {
        address1.addEvent('change', function() { webtrendsEventTracking.onAddressChange(address1); });
    }

    var town = $('mPay-billingCity');
    if (town) {
        town.addEvent('change', function() { webtrendsEventTracking.onTownChange(town); });
    }

    var cardType = $('mPay-cardType');
    if (cardType) {
        cardType.addEvent('change', function() { webtrendsEventTracking.onCardTypeChange(cardType); });
    }

    var cardNumber = $('mPay-cardNumber');
    if (cardNumber) {
        cardNumber.addEvent('change', function() { webtrendsEventTracking.onCardNumberChange(cardNumber); });
    }

    var cardName = $('mPay-cardName');
    if (cardName) {
        cardName.addEvent('change', function() { webtrendsEventTracking.onNameOnCardChange(cardName); });
    }

    var startDateMonth = $('creditcard-startmonth');
    if (startDateMonth) {
        startDateMonth.addEvent('change', function() { webtrendsEventTracking.onStartDateMMChange(startDateMonth); });
    }

    var startDateYear = $('creditcard-startyear');
    if (startDateYear) {
        startDateYear.addEvent('change', function() { webtrendsEventTracking.onStartDateYYChange(startDateYear); });
    }

    var expiryDateMonth = $('creditcard-endmonth');
    if (expiryDateMonth) {
        expiryDateMonth.addEvent('change', function() { webtrendsEventTracking.onExpiryDateMMChange(expiryDateMonth); });
    }

    var expiryDateYear = $('creditcard-endyear');
    if (expiryDateYear) {
        expiryDateYear.addEvent('change', function() { webtrendsEventTracking.onExpiryDateYYChange(expiryDateYear); });
    }

    var secCode = $('mPay-cardCvv');
    if (secCode) {
        secCode.addEvent('change', function() { webtrendsEventTracking.onSecurityCodeChange(secCode); });
    }

    var chkTerms = $('terms-checkbox');
    if (chkTerms) {
        chkTerms.addEvent('click', function() { webtrendsEventTracking.onConfirmTcsChange(chkTerms); });
    }

    //Only add to the first guest fields, not bothered about any more guests
    var guestTitle = $('roomtitle0');
    if (guestTitle) {
        guestTitle.addEvent('change', function() { webtrendsEventTracking.onTitleChange(guestTitle); });
    }

    var guestFirstname = $('firstname0');
    if (guestFirstname) {
        guestFirstname.addEvent('change', function() { webtrendsEventTracking.onFirstNameChange(guestFirstname); });
    }

    var guestSurname = $('lastname0');
    if (guestSurname) {
        guestSurname.addEvent('change', function() { webtrendsEventTracking.onSurnameChange(guestSurname); });
    }

    var guestSendConf = $('guestemailconfirm0');
    if (guestSendConf) {
        guestSendConf.addEvent('click', function() { webtrendsEventTracking.onSendConfirmEmailChange(guestSendConf); });
    }

    var guestEmail = $('guest-email-0');
    if (guestEmail) {
        guestEmail.addEvent('change', function() { webtrendsEventTracking.onEmailAddressChange(guestEmail); });
    }

    var guestConfirmEmail = $('guest-email-confirm-0');
    if (guestConfirmEmail) {
        guestConfirmEmail.addEvent('click', function() { webtrendsEventTracking.onConfirmEmailAddressChange(guestConfirmEmail); });
    }
});

