﻿var webtrendsEventTracking = {
    bookingIsForBooker: function() {
        var bookerOrGuestCheckbox = $$('input[name=bookingInformation.BookerOrGuest]:checked')[0];
        var isBooker = !bookerOrGuestCheckbox || (bookerOrGuestCheckbox.get('value') == 'Booker') ? true : false;
        return isBooker;
    },

    onTitleChange: function(element) {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();
        var isRoomGuestField = ((element.id).indexOf('ddlGuestTitle') > -1);

        var dcsuri = isBooker ? '/bookingform/me/title' : (isRoomGuestField ? '/bookingform/someoneelse/title_room' : '/bookingform/someoneelse/title');
        var title = isBooker ? 'Title (Form 1)' : (isRoomGuestField ? 'Title Room Details (Form 2)' : 'Title (Form 2)');
        var scenarioName = isBooker ? 'BookingForm1;FullBookingForm1' : 'BookingForm2;FullBookingForm2';
        var scenarioNumber = isBooker || isRoomGuestField ? '1;1' : '3;8';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onFirstNameChange: function(element) {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();
        var isRoomGuestField = ((element.id).indexOf('txtGuestFirstName') > -1);

        var dcsuri = isBooker ? '/bookingform/me/firstname' : (isRoomGuestField ? '/bookingform/someoneelse/firstname_room' : '/bookingform/someoneelse/firstname');
        var title = isBooker ? 'First Name (Form 1)' : (isRoomGuestField ? 'First Name Room Details (Form 2)' : 'First Name (Form 2)');
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker || isRoomGuestField ? '2' : '9';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onSurnameChange: function(element) {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();
        var isRoomGuestField = ((element.id).indexOf('txtGuestSurname') > -1);

        var dcsuri = isBooker ? '/bookingform/me/surname' : (isRoomGuestField ? '/bookingform/someoneelse/surname_room' : '/bookingform/someoneelse/surname');
        var title = isBooker ? 'Surname (Form 1)' : (isRoomGuestField ? 'Surname Room Details (Form 2)' : 'Surname (Form 2)');
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker || isRoomGuestField ? '3' : '10';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onEmailAddressChange: function(element) {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();
        var isRoomGuestField = ((element.id).indexOf('txtGuestEmail') > -1);

        var dcsuri = isBooker ? '/bookingform/me/emailaddress' : (isRoomGuestField ? '/bookingform/someoneelse/emailaddress_room' : '/bookingform/someoneelse/email');
        var title = isBooker ? 'Email Address (Form 1)' : (isRoomGuestField ? 'Email Address Details (Form 2)' : 'Email (Form 2)');
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker || isRoomGuestField ? '4' : '11';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onConfirmEmailAddressChange: function(element) {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();
        var isRoomGuestField = ((element.id).indexOf('txtGuestConfirmEmail') > -1);

        var dcsuri = isBooker ? '/bookingform/me/confirmemail' : (isRoomGuestField ? '/bookingform/someoneelse/confirmemail_room' : '/bookingform/someoneelse/confirmemail');
        var title = isBooker ? 'Confirm Email Address (Form 1)' : (isRoomGuestField ? 'Confirm Email Address Details (Form 2)' : 'Confirm Email Address (Form 2)');
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker || isRoomGuestField ? '5' : '12';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onSendConfirmEmailChange: function() {
        var dcsuri = '/bookingform/someoneelse/guestconfirmemail_room';
        var title = 'Send Guest Confirmation Email Details (Form 2)';
        var scenarioName = 'FullBookingForm2';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', '6');
    },

    onCountryChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/country' : '/bookingform/someoneelse/country';
        var title = isBooker ? 'Country (Form 1)' : 'Country (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '6' : '13';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onPostcodeChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/postcode' : '/bookingform/someoneelse/postcode';
        var title = isBooker ? 'Postcode (Form 1)' : 'Postcode (Form 2)';
        var scenarioName = isBooker ? 'BookingForm1;FullBookingForm1' : 'BookingForm2;FullBookingForm2';
        var scenarioNumber = isBooker ? '2;7' : '4;14';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onAddressChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/Address1' : '/bookingform/someoneelse/Address1';
        var title = isBooker ? 'Address1 (Form 1)' : 'Address1 (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '8' : '15';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onTownChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/town' : '/bookingform/someoneelse/town';
        var title = isBooker ? 'Town (Form 1)' : 'Town (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '9' : '16';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onTelephoneChange: function(element) {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();
        var isRoomGuestField = ((element.id).indexOf('txtGuestTelephone') > -1);

        var dcsuri = isBooker ? '/bookingform/me/telephone' : (isRoomGuestField ? '/bookingform/someoneelse/telephone_room' : '/bookingform/someoneelse/telephone');
        var title = isBooker ? 'Telephone Number (Form 1)' : (isRoomGuestField ? 'Telephone Room Details (Form 2)' : 'Telephone Number (Form 2)');
        var scenarioName = isBooker ? 'FullBookingForm1' : isRoomGuestField ? 'BookingForm2;FullBookingForm2' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '10' : isRoomGuestField ? '2;7' : '17';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onRememberMeChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/rememberme' : '/bookingform/someoneelse/rememberme';
        var title = isBooker ? 'Remember Me (Form 1)' : 'Remember Me (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '11' : '18';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onCardTypeChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/cardtype' : '/bookingform/someoneelse/cardtype';
        var title = isBooker ? 'Card Type (Form 1)' : 'Card Type (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '12' : '19';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onCardNumberChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/cardnumber' : '/bookingform/someoneelse/cardnumber';
        var title = isBooker ? 'Card Number (Form 1)' : 'Card Number (Form 2)';
        var scenarioName = isBooker ? 'BookingForm1;FullBookingForm1' : 'BookingForm2;FullBookingForm2';
        var scenarioNumber = isBooker ? '3;13' : '5;20';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onNameOnCardChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/nameoncard' : '/bookingform/someoneelse/nameoncard';
        var title = isBooker ? 'Name on Card (Form 1)' : 'Name on Card (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '14' : '21';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onStartDateMMChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/startdatemm' : '/bookingform/someoneelse/startdatemm';
        var title = isBooker ? 'Start Date MM (Form 1)' : 'Start Date MM (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '15' : '22';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onStartDateYYChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/startdateyy' : '/bookingform/someoneelse/startdateyy';
        var title = isBooker ? 'Start Date YY (Form 1)' : 'Start Date YY (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '16' : '23';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onExpiryDateMMChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/expirymm' : '/bookingform/someoneelse/expirymm';
        var title = isBooker ? 'Expiry Date MM (Form 1)' : 'Expiry Date MM (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '17' : '24';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onExpiryDateYYChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/expiryyy' : '/bookingform/someoneelse/expiryyy';
        var title = isBooker ? 'Expiry Date YY (Form 1)' : 'Expiry Date YY (Form 2)';
        var scenarioName = isBooker ? 'FullBookingForm1' : 'FullBookingForm2';
        var scenarioNumber = isBooker ? '18' : '25';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onSecurityCodeChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/securitycode' : '/bookingform/someoneelse/securitycode';
        var title = isBooker ? 'Security Code (Form 1)' : 'Security Code (Form 2)';
        var scenarioName = isBooker ? 'BookingForm1;FullBookingForm1' : 'BookingForm2;FullBookingForm2';
        var scenarioNumber = isBooker ? '4;19' : '6;26';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },
    onConfirmTcsChange: function() {
        var isBooker = webtrendsEventTracking.bookingIsForBooker();

        var dcsuri = isBooker ? '/bookingform/me/confirmtcs' : '/bookingform/someoneelse/confirmtcs';
        var title = isBooker ? 'Confirm Terms and Conditions (Form 1)' : 'Confirm Terms and Conditions (Form 2)';
        var scenarioName = isBooker ? 'BookingForm1;FullBookingForm1' : 'BookingForm2;FullBookingForm2';
        var scenarioNumber = isBooker ? '5;20' : '7;27';

        dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
    },

    onSubmit: function() {

        try {
            var isBooker = webtrendsEventTracking.bookingIsForBooker();
            var dcsuri = isBooker ? '/bookingform/me/submit' : '/bookingform/someoneelse/submit';
            var title = isBooker ? 'Submit Booking (Form 1)' : 'Submit Booking (Form 2)';
            var scenarioName = isBooker ? 'BookingForm1;FullBookingForm1' : 'BookingForm2;FullBookingForm2';
            var scenarioNumber = isBooker ? '6;21' : '8;28';

            dcsMultiTrack('DCS.dcsuri', dcsuri, 'WT.ti', title, 'WT.dl', '99', 'WT.si_n', scenarioName, 'WT.si_x', scenarioNumber);
        }
        catch (e) {
            // suppress exception as we don't want to block submitting the booking
        }
        finally {
            return true;
        }
    }
};






window.addEvent('domready', function() {


    var ddlTitle = $('ddlTitle');
    if (ddlTitle) {
        ddlTitle.addEvent('change', function() { webtrendsEventTracking.onTitleChange(ddlTitle); });
    }

    var txtFirstName = $('txtFirstName');
    if (txtFirstName) {
        txtFirstName.addEvent('change', function() { webtrendsEventTracking.onFirstNameChange(txtFirstName); });
    }

    var txtSurname = $('txtSurname');
    if (txtSurname) {
        txtSurname.addEvent('change', function() { webtrendsEventTracking.onSurnameChange(txtSurname); });
    }

    var txtEmail = $('txtEmail');
    if (txtEmail) {
        txtEmail.addEvent('change', function() { webtrendsEventTracking.onEmailAddressChange(txtEmail); });
    }

    var txtConfirmEmail = $('txtConfirmEmail');
    if (txtConfirmEmail) {
        txtConfirmEmail.addEvent('change', function() { webtrendsEventTracking.onConfirmEmailAddressChange(txtConfirmEmail); });
    }

    var txtConEmail = $('txtConemail');
    if (txtConEmail) {
        txtConEmail.addEvent('change', function() { webtrendsEventTracking.onConfirmEmailAddressChange(txtConEmail); });
    }

    var ddlCountry = $('ddlCountry');
    if (ddlCountry) {
        ddlCountry.addEvent('change', function() { webtrendsEventTracking.onCountryChange(ddlCountry); });
    }

    var postcode = $('Address_Postcode');
    if (postcode) {
        postcode.addEvent('change', function() { webtrendsEventTracking.onPostcodeChange(postcode); });
    }

    var address1 = $('Address_Address1');
    if (address1) {
        address1.addEvent('change', function() { webtrendsEventTracking.onAddressChange(address1); });
    }

    var town = $('Address_Town');
    if (town) {
        town.addEvent('change', function() { webtrendsEventTracking.onTownChange(town); });
    }

    var telephone = $('txtTelephone');
    if (telephone) {
        telephone.addEvent('change', function() { webtrendsEventTracking.onTelephoneChange(telephone); });
    }

    var rememberMe = $('chkRemember');
    if (rememberMe) {
        rememberMe.addEvent('change', function() { webtrendsEventTracking.onRememberMeChange(rememberMe); });
    }

    var cardType = $('ddlCardType');
    if (cardType) {
        cardType.addEvent('change', function() { webtrendsEventTracking.onCardTypeChange(cardType); });
    }

    var cardNumber = $('txtCardNumber');
    if (cardNumber) {
        cardNumber.addEvent('change', function() { webtrendsEventTracking.onCardNumberChange(cardNumber); });
    }

    var cardName = $('txtCardName');
    if (cardName) {
        cardName.addEvent('change', function() { webtrendsEventTracking.onNameOnCardChange(cardName); });
    }

    var startDateMonth = $('ddlStartDateMonth');
    if (startDateMonth) {
        startDateMonth.addEvent('change', function() { webtrendsEventTracking.onStartDateMMChange(startDateMonth); });
    }

    var startDateYear = $('ddlStartDateYear');
    if (startDateYear) {
        startDateYear.addEvent('change', function() { webtrendsEventTracking.onStartDateYYChange(startDateYear); });
    }

    var endDateMonth = $('ddlEndDateMonth');
    if (endDateMonth) {
        endDateMonth.addEvent('change', function() { webtrendsEventTracking.onExpiryDateMMChange(endDateMonth); });
    }
    
    var expiryDateMonth = $('ddlExpiryDateMonth');
    if (expiryDateMonth) {
        expiryDateMonth.addEvent('change', function() { webtrendsEventTracking.onExpiryDateMMChange(expiryDateMonth); });
    }

    var endDateYear = $('ddlEndDateYear');
    if (endDateYear) {
        endDateYear.addEvent('change', function() { webtrendsEventTracking.onExpiryDateYYChange(endDateYear); });
    }
    
    var expiryDateYear = $('ddlExpiryDateYear');
    if (expiryDateYear) {
        expiryDateYear.addEvent('change', function() { webtrendsEventTracking.onExpiryDateYYChange(expiryDateYear); });
    }

    var secCode = $('txtSecCode');
    if (secCode) {
        secCode.addEvent('change', function() { webtrendsEventTracking.onSecurityCodeChange(secCode); });
    }

    var chkTerms = $('chkTerms');
    if (chkTerms) {
        chkTerms.addEvent('change', function() { webtrendsEventTracking.onConfirmTcsChange(chkTerms); });
    }

    //Only add to the first guest fields, not bothered about any more guests
    var guestTitle = $('ddlGuestTitle0');
    if (guestTitle) {
        guestTitle.addEvent('change', function() { webtrendsEventTracking.onTitleChange(guestTitle); });
    }

    var guestFirstname = $('txtGuestFirstName0');
    if (guestFirstname) {
        guestFirstname.addEvent('change', function() { webtrendsEventTracking.onFirstNameChange(guestFirstname); });
    }
    
    var guestSurname = $('txtGuestSurname0');
    if (guestSurname) {
        guestSurname.addEvent('change', function() { webtrendsEventTracking.onSurnameChange(guestSurname); });
    }
    
    var guestEmail = $('txtGuestEmail0');
    if (guestEmail) {
        guestEmail.addEvent('change', function() { webtrendsEventTracking.onEmailAddressChange(guestEmail); });
    }

    var guestSendConf = $('chkGuestSendConf0');
    if (guestSendConf) {
        guestSendConf.addEvent('change', function() { webtrendsEventTracking.onSendConfirmEmailChange(guestSendConf); });
    }
    
    var guestConfirmEmail = $('txtGuestConfirmEmail0');
    if (guestConfirmEmail) {
        guestConfirmEmail.addEvent('change', function() { webtrendsEventTracking.onConfirmEmailAddressChange(guestConfirmEmail); });
    }
    
    var guestTelephone = $('txtGuestTelephone0');
    if (guestTelephone) {
        guestTelephone.addEvent('change', function() { webtrendsEventTracking.onTelephoneChange(guestTelephone); });
    }
});

