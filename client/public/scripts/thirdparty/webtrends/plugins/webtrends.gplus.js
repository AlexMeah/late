﻿/*
Copyright (c) 2012 Webtrends, Inc.
Google+ Plugin v1.0.0

An example of including the plugin with your tag.

<script type="text/javascript">
// async loader function, called by webtrends.js after load
window.webtrendsAsyncInit=function(){
var dcs=new Webtrends.dcs().init({
dcsid:"YOUR_WEBTRENDS_DCSID_HERE"
,timezone:YOUR_TIMEZONE_HERE
,plugins:{
plugins:{gplus:{ src:"webtrends.gplus.js"}}
}
}).track();
};
(function(){
var s=document.createElement("script"); s.async=true; s.src="webtrends.js";    
var s2=document.getElementsByTagName("script")[0]; s2.parentNode.insertBefore(s,s2);
}());
</script>


In your Google Plus One button, set callback="WebTrendsGPlusTrack"
<g:plusone size="tall" annotation="inline" callback="WebTrendsGPlusTrack" href="http://www.site.com"></g:plusone>


The track() function will return 'true' when it tracks data, 'false' otherwise.


*/
var WebTrendsGPlusTrack, WebTrendsGPlus;

(function() {
    WebTrendsGPlusTrack = function(obj) {

        if (!_wtgplus) {
            return false;
        }

        var gplusData = { 'args': {} };

        gplusData['args']['WT.soc_action'] = "Google: +1";
        var url = obj.href;
        if (url.length > 300) {
            url = url.substring(0, 300);
        }
        gplusData['args']['WT.soc_content'] = url;

        _wtgplus.tagObj.dcsMultiTrack(gplusData);

        return true;
    };

    WebTrendsGPlus = function(tag) {
        this.enabled = true;
        this.tagObj = tag;
    };
})();

var _wtgplus;

var gplus_loader = function(tag) {
    _wtgplus = new WebTrendsGPlus(tag);
};

Webtrends.registerPlugin('gplus', gplus_loader);