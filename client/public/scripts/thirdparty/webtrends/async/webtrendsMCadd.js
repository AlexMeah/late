// Multi-Currency plugin for Webtrends v10 Async tag
// Requires webtrendsMC_config.js to be loaded first
// Also requires webtrendsMC.js plugin
(function() {  
	if (!window.Webtrends){  
		return; // ensuring Webtrends has loaded  
	}
	
	// This makes use of functions present in webtrendsMC.js plugin, which is designed for purchases.
	// Rather than duplicate the functionality or modify taggign, an add-to-basket event is initially 
	// treated as a purchase, then the parameters are renamed:
	//      WT.tx_s > WT.tx_sa     WT.tx_s_rollup > WT.tx_sa_rollup     WT.tx_s_txn > WT.tx_sa_txn
	function mca_transform(dcs, o) {
		try {
			// We might have params already in dcs from page load, 
			// or in o.argsa from dcsMultiTrack call (which haven't yet been pushed into dcs). 
			var params = mc_mergeParams(dcs, o.argsa);
			// We only care about all this if there is a WT.tx_e and it is a purchase
			if (typeof(params['WT.tx_e']) != 'undefined' && params['WT.tx_e'].toLowerCase() == 'a') {
				mc_populateCurrencies(params);
				// Need to rename WT.tx_s... parameters so as not to confuse with orders
				for (var param in params) {
					if (param.search(/^WT\.tx_s($|_)/) == 0) {
						params[param.replace('WT.tx_s', 'WT.tx_sa')] = params[param];
						params[param] = '';
					}
				}
				mc_updateArgsa(params, o.argsa);
			}
		}
		catch (e) {
			// Just set the error in the parameter and set WT.tx_sa (etc) to zero and clear any tx_s values
			o.argsa.push('WT.tx_error', e.message);
			
			// If we don't have a tx_s, then shove in zeroes for tx_sa values
			if (typeof(params['WT.tx_s']) == 'undefined') {
				o.argsa.push('WT.tx_s', '', 'WT.tx_s_rollup', '', 'WT.tx_s_txn', '', 'WT.tx_sa', '0.00', 'WT.tx_sa_rollup', '0.00', 'WT.tx_sa_txn', '0.00');
			}
			// If we do have tx_s, then pad with the right number of zeroes and clear any tx_s values
			else {
				var vals = params['WT.tx_s'].split(';');
				for (var i=0; i<vals.length; i++) {
					vals[i] = '0.00';
				}
				o.argsa.push('WT.tx_s', '', 'WT.tx_s_rollup', '', 'WT.tx_s_txn', '', 'WT.tx_sa_txn', params['WT.tx_s'], 'WT.tx_sa', vals.join(';'), 'WT.tx_sa_rollup', vals.join(';'));
			}
		}
	};
	
	
	function mca_loader(dcs, o) {
		try {
			dcs.addTransform(mca_transform, 'all');
		}
		catch (e) {}
	};
	  
	
	// explicity callback we are done loading   
	Webtrends.registerPlugin('mca', mca_loader);  
	  
})(window.Webtrends);  
