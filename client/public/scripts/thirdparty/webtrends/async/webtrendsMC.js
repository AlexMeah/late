// Multi-Currency plugin for Webtrends v10 Async tag
// Requires webtrendsMC_config.js to be loaded first
(function() {  
	if (!window.Webtrends){  
		return; // ensuring Webtrends has loaded  
	}
	
	// Adds in a method for formatting currency figures
	mc_formatMoney = function(amount) {
		return parseFloat(amount).toFixed(2).toString();
	};
	
	
	// Adds in a method to our custom object for mc_converting between currencies
	mc_convert = function(amount, sourceCurrency, targetCurrency) {
		var rate = _tag.exRates[sourceCurrency + '-' + targetCurrency];
		// If no rate is specified, will throw the error
		if (typeof(rate) == 'undefined') {
			throw (new Error("_tag.exRates['" + sourceCurrency + "-" + targetCurrency + "'] undefined"));
		}
		return mc_formatMoney(amount * rate);
	};

	
	//Adds in a method to our custom object for populating all multi-currency transaction details
	mc_populateCurrencies = function(params) {
		// WT.tx_error is used to record any errors, so we make sure it's cleared first, 
		// along with any previous values for WT.tx_s_rollup and WT.tx_s_txn
		params['WT.tx_error'] = '';
		params['WT.tx_s_rollup'] = '';
		params['WT.tx_s_txn'] = '';
		
		// Check if WT.tx_s is specified (product subtotals)
		if (typeof(params['WT.tx_s']) == 'undefined') {
			throw (new Error('WT.tx_s is undefined'));
		}
		// Check if WT.tx_curr is specified (transaction currency)
		if (typeof(params['WT.tx_curr']) == 'undefined') {
			throw (new Error('WT.tx_curr is undefined'));
		}
		// Check if _tag.rollupCurrency is specified
		if (typeof(_tag.rollupCurrency) == 'undefined') {
			throw (new Error('_tag.rollupCurrency is undefined'));
		}
		// Find the site currency if we can. If no sitecode parameter is defined, then we
		// don't have a multi-site implementation, so set siteCurr to rollupCurrency
		if (typeof(_tag.siteCodeParam) == 'undefined') {
			var siteCurr = _tag.rollupCurrency;
		}
		// If a sitecode param is specified, but not present, then record error
		else if (typeof(params[_tag.siteCodeParam]) == 'undefined') {
			throw (new Error(_tag.siteCodeParam + ' is undefined'));
		} 
		// Sitecode param is specified and present, but we can't find the site's currency
		else if (typeof(_tag.siteCurrencies[params[_tag.siteCodeParam]]) == 'undefined') {
			throw (new Error("_tag.siteCurrencies['" + params[_tag.siteCodeParam] + "'] is undefined"));
		}
		// All OK
		else {
			var siteCurr = _tag.siteCurrencies[params[_tag.siteCodeParam]].toUpperCase();
		}
			
		// Now we should have everything we need
		var aValueTrans = params['WT.tx_s'].split(';');
		var aValueSite = new Array(aValueTrans.length);
		var aValueRollup = new Array(aValueTrans.length);
		
		// First, make sure passed transaction values are properly formatted
		for (var i=0; i<aValueTrans.length; i++) {
			aValueTrans[i] = mc_formatMoney(aValueTrans[i]);
		}
		
		// If site currency is same as transaction, then no conversion needed
		if (params['WT.tx_curr'] == siteCurr) {
			aValueSite = aValueTrans;
		}
		else {
			// mc_convert transaction values to site currency
			for (i=0; i<aValueTrans.length; i++) {
				aValueSite[i] = mc_convert(aValueTrans[i], params['WT.tx_curr'], siteCurr);
			}
		}

		// If transaction currency is rollup currency then no conversion needed
		if (params['WT.tx_curr'] == _tag.rollupCurrency) {
			aValueRollup = aValueTrans;
		}
		else {
			// Or if site currency is rollup currency, then no conversion needed
			if (siteCurr == _tag.rollupCurrency) {
				aValueRollup = aValueSite;
			}
			else {
				// convert transaction values to rollup currency
				for (i=0; i<aValueTrans.length; i++) {
					aValueRollup[i] = mc_convert(aValueTrans[i], params['WT.tx_curr'], _tag.rollupCurrency);
				}
			}
		}

		// Now put the values into WebTrends tags
		params['WT.tx_s'] = aValueSite.join(';');
		params['WT.tx_s_rollup'] = aValueRollup.join(';');
		params['WT.tx_s_txn'] = aValueTrans.join(';');
	};
	
	
	// Merge page-level and dcsMultiTrack parameters together
	mc_mergeParams = function(dcs, argsa) {
		// These are the params we're interested in for multi-currency
		var params = new Object();
		params['WT.tx_e'] = '';
		params['WT.tx_s'] = '';
		params['WT.tx_curr'] = '';
		// If we've got a config item for siteCodeParam, and there is a value for it, then set in params.
		if (typeof(_tag.siteCodeParam) != 'undefined') {
			params[_tag.siteCodeParam] = '';
		}
		
		// Go through these params and pull out any existing values from the dcs object
		for (var param in params) {
			params[param] = eval('dcs.' + param);
		}
	
		// Go through any new dcsMultiTrack args and update params with these values (will add or overwrite as appropriate)
		for (var i = 0; i < argsa.length; i += 2) {
			if (argsa[i] in params) {
				params[argsa[i]] = argsa[i + 1];
			}
		}
		
		return params;
	};
	
	
	// Update the argsa array with anything we've set for multi-currency
	mc_updateArgsa = function(params, argsa) {
		for (var param in params) {
			// We don't need to update tx_e or the site code (usually WT.sp), so let's get rid of them
			delete param['WT.tx_e'];
			delete param[_tag.siteCodeParam];
			// Push the values we have onto the argsa array. 
			// If these duplicate previous values, that's OK because they will overwrite when the array is processed.
			argsa.push(param, params[param]);
		}
	};
	
	
	function mc_transform(dcs, o) {
		try {
			// We might have purchase params already in dcs from page load, 
			// or in o.argsa from dcsMultiTrack call (which haven't yet been pushed into dcs). 
			var params = mc_mergeParams(dcs, o.argsa);
			// We only care about all this if there is a WT.tx_e and it is a purchase
			if (typeof(params['WT.tx_e']) != 'undefined' && params['WT.tx_e'].toLowerCase() == 'p') {
				mc_populateCurrencies(params);
				mc_updateArgsa(params, o.argsa);
			}
		}
		catch (e) {
			// Just set the error in the parameter and set WT.tx_s (etc) to zero
			o.argsa.push('WT.tx_error', e.message);
			
			// If we don't have a tx_s, then shove in zeroes
			if (typeof(params['WT.tx_s']) == 'undefined') {
				o.argsa.push('WT.tx_s', '0.00', 'WT.tx_s_rollup', '0.00', 'WT.tx_s_txn', '0.00');
			}
			// If we do have tx_s, then pad with the right number of zeroes
			else {
				var vals = params['WT.tx_s'].split(';');
				for (var i=0; i<vals.length; i++) {
					vals[i] = '0.00';
				}
				o.argsa.push('WT.tx_s_txn', params['WT.tx_s'], 'WT.tx_s', vals.join(';'), 'WT.tx_s_rollup', vals.join(';'));
			}
		}
	};
	
	
	function mc_loader(dcs, o) {
		try {
			dcs.addTransform(mc_transform, 'all');
		}
		catch (e) {}
	};
	  
	
	// explicity callback we are done loading   
	Webtrends.registerPlugin('mc', mc_loader);  
	  
})(window.Webtrends);  
