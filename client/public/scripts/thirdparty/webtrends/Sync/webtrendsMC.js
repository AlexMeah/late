// This effectively extends the standard WebTrends object
function WebTrendsMC() {
	// Create the standard tag in here
	this.wtTag = new WebTrends();
	
	// Reference all tag properties/methods into this custom object so they can all be used as normal. 
	// (Although be aware simple data types are passed by value, so will not reflect any changes made inside wtTag)
	for (var n in this.wtTag) {
		this[n] = this.wtTag[n];
	}

	// Prepare for the currency info
	this.exRates = new Object();
	this.siteCurrencies = new Object();
	
	// This overrides the standard dcsTag to pre-process all transaction currency conversions (works on load and dcsMultiTrack()
	this.dcsTag = function() {
		this.populateCurrencies();
		this.wtTag.dcsTag(arguments);
	};

	// This overrides the standard dcsCleanUp to ensure references are maintained
	this.dcsCleanUp = function() {
		this.wtTag.dcsCleanUp(arguments);
		this.DCS = this.wtTag.DCS;
		this.DCSext = this.wtTag.DCSext;
		this.WT = this.wtTag.WT;
	};
}

// Adds in a method to our custom object for formatting currency figures
WebTrendsMC.prototype.formatMoney = function(amount) {
	return parseFloat(amount).toFixed(2).toString();
};

// Adds in a method to our custom object for converting between currencies
WebTrendsMC.prototype.convert = function(amount, sourceCurrency, targetCurrency) {
	var rate = this.exRates[sourceCurrency + '-' + targetCurrency];
	// If no rate is specified, will throw the error
	if (typeof(rate) == 'undefined') {
		throw (new Error("_tag.exRates['" + sourceCurrency + "-" + targetCurrency + "'] undefined"));
	}
	return this.formatMoney(amount * rate);
};

//Adds in a method to our custom object for populating all multi-currency transaction details
WebTrendsMC.prototype.populateCurrencies = function () {
	// WT.tx_error is used to record any errors, so we make sure it's cleared first, 
	// along with any previous values for WT.tx_s_rollup and WT.tx_s_txn
	this.WT.tx_error = null;
	this.WT.tx_s_rollup = null;
	this.WT.tx_s_txn = null;
	
	// We only care about all this is there is a WT.tx_e and it is a purchase
	if (typeof(this.WT.tx_e) != 'undefined' && this.WT.tx_e.toLowerCase() == 'p') {
		try {
			// Check if WT.tx_s is specified (product subtotals)
			if (typeof(this.WT.tx_s) == 'undefined') {
				throw (new Error('WT.tx_s is undefined'));
			}
			// Check if WT.tx_curr is specified (transaction currency)
			if (typeof(this.WT.tx_curr) == 'undefined') {
				throw (new Error('WT.tx_curr is undefined'));
			}
			// Check if _tag.rollupCurrency is specified
			if (typeof(this.rollupCurrency) == 'undefined') {
				throw (new Error('_tag.rollupCurrency is undefined'));
			}
			// Find the site currency if we can. If no sitecode parameter is defined, then we
			// don't have a multi-site implementation, so set siteCurr to rollupCurrency
			if (typeof(this.siteCodeParam) == 'undefined') {
				var siteCurr = this.rollupCurrency;
			}
			// If a sitecode param is specified, but not present, then record error
			else if (typeof(eval('this.' + this.siteCodeParam)) == 'undefined') {
				throw (new Error(this.siteCodeParam + ' is undefined'));
			} 
			// Sitecode param is specified and present, but we can't find the site's currency
			else if (typeof(this.siteCurrencies[eval('this.' + this.siteCodeParam)]) == 'undefined') {
				throw (new Error("_tag.siteCurrencies['" + eval('this.' + this.siteCodeParam) + "'] is undefined"));
			}
			// All OK
			else {
				var siteCurr = this.siteCurrencies[eval('this.' + this.siteCodeParam)].toUpperCase();
			}
				
			// Now we should have everything we need
			var aValueTrans = this.WT.tx_s.split(';');
			var aValueSite = new Array(aValueTrans.length);
			var aValueRollup = new Array(aValueTrans.length);
			
			// First, make sure passed transaction values are properly formatted
			for (var i=0; i<aValueTrans.length; i++) {
				aValueTrans[i] = this.formatMoney(aValueTrans[i]);
			}
			
			// If site currency is same as transaction, then no conversion needed
			if (this.WT.tx_curr == siteCurr) {
				aValueSite = aValueTrans;
			}
			else {
				// Convert transaction values to site currency
				for (i=0; i<aValueTrans.length; i++) {
					aValueSite[i] = this.convert(aValueTrans[i], this.WT.tx_curr, siteCurr);
				}
			}

			// If transaction currency is rollup currency then no conversion needed
			if (this.WT.tx_curr == this.rollupCurrency) {
				aValueRollup = aValueTrans;
			}
			else {
				// Or if site currency is rollup currency, then no conversion needed
				if (siteCurr == this.rollupCurrency) {
					aValueRollup = aValueSite;
				}
				else {
					// Convert transaction values to rollup currency
					for (i=0; i<aValueTrans.length; i++) {
						aValueRollup[i] = this.convert(aValueTrans[i], this.WT.tx_curr, this.rollupCurrency);
					}
				}
			}

			// Now put the values into WebTrends tags
			this.WT.tx_s = aValueSite.join(';');
			this.WT.tx_s_rollup = aValueRollup.join(';');
			this.WT.tx_s_txn = aValueTrans.join(';');
		}
		
		catch (e) {
			// Just set the error in the parameter and set WT.tx_s (etc) to zero
			this.WT.tx_error = e.message;
			if (typeof(this.WT.tx_s) == 'undefined') {
				this.WT.tx_s = '0.00';
				this.WT.tx_s_rollup = '0.00';
				this.WT.tx_s_txn = '0.00';
			}
			else {
				var vals = this.WT.tx_s.split(';');
				for (var i=0; i<vals.length; i++) {
					vals[i] = '0.00';
				}
				this.WT.tx_s_txn = this.WT.tx_s;
				this.WT.tx_s = vals.join(';');
				this.WT.tx_s_rollup = this.WT.tx_s;
			}
		}
	}
};
