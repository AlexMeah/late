// The name of the Webtrends query parameter to use in identifying the site.
// Omit this line if you do not have multiple sites with different currencies.
_tag.siteCodeParam = 'WT.sp';

// The standard currency for each website (code in [] corresponds to values in 
// siteCodeParam). Omit this section if you do not have multiple sites
_tag.siteCurrencies['LateRoomsFR'] = 'EUR';
_tag.siteCurrencies['LateRoomsEN'] = 'GBP';
_tag.siteCurrencies['LateRoomsES'] = 'EUR';
_tag.siteCurrencies['LateRoomsDE'] = 'EUR';
_tag.siteCurrencies['LateRoomsIT'] = 'EUR';
_tag.siteCurrencies['LateRoomsAU'] = 'AUD';