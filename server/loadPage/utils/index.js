var utils = {};

utils.hasLib = function(path) {
    try {
        require(path + '/lib');
    } catch (err) {
        if (err.code === 'MODULE_NOT_FOUND') {
            return false;
        }
    }

    return true;
};

utils.has = function(object, key) {
    return Object.prototype.hasOwnProperty.call(object, key);
};

utils.merge = function() {
    var process = function(target, source) {
        for (var key in source) {
            if (utils.has(source, key)) {
                target[key] = source[key];
            }
        }
        return target;
    };
    var result = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        result = process(result, arguments[i]);
    }
    return result;
};

module.exports = utils;