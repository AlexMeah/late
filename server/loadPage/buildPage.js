var fs = require('fs');
var path = require('path');
var async = require('async');
var utils = require('./utils');
var Promise = require('promise');
var handlebars = require('handlebars');
var helpers = require('./utils/helpers');


var handleError = function(req, res, err) {
    console.log(err);
    res.statusCode = 500;
    res.setHeader('content-type', 'text/json');

    res.end(JSON.stringify(err, null, 4));
};

var buildPage = function(req, res, next) {
    var pageJson = res.pageJson;
    var sections = Object.keys(pageJson.slots);
    var builtSections = {};

    function buildComponent(componentData, callback) {
        var componentPath = path.resolve(path.join('./components/', componentData.type));

        /*
         * Use a promise to make sure we wait for any api
         * calls in lib to be resolved
         */

        var promise = new Promise(function(resolve, reject) {
            if (!utils.hasLib(componentPath)) {
                resolve(null);
            } else {
                var lib = require(path.join(componentPath, 'lib'));

                /*
                 * Send the req as well as a method to resolve or reject to the lib
                 */

                lib(req, resolve, reject);
            }
        });

        promise
            .then(
                function(libdata) {
                    fs.readFile(path.join(componentPath, 'index.hbs'), 'utf8', function(err, data) {
                        if (err) {
                            return handleError(req, res, err);
                        }

                        var template = handlebars.compile(data);

                        callback(null, template(utils.merge(componentData, req.params, libdata)));
                    });
                },
                function(err) {
                    return handleError(req, res, err);
                }
        );
    };

    function buildSection(section, callback) {
        async.map(pageJson.slots[section].components, buildComponent, function(err, results) {
            if (err) {
                return handleError(req, res, err);
            }

            builtSections[section] = results.join('\n');

            callback();
        });
    }

    async.each(sections, buildSection, function(err) {
        if (err) {
            return handleError(req, res, err);
        }

        var layoutPath = path.resolve(path.join('./components/', pageJson.type));
        fs.readFile(path.join(layoutPath, 'index.hbs'), 'utf8', function(err, data) {
            if (err) {
                handleError(req, res, err);
            }

            var template = handlebars.compile(data);

            res.statusCode = 200;
            res.setHeader('content-type', 'text/html');

            res.end(template(utils.merge(builtSections, req.params)));
        });
    });
};

module.exports = buildPage;