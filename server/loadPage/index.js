var utils = require('./utils');
var buildPage = require('./buildPage');
var jsonApi = require('../api/jsonApi');

var loadPage = function(req, res, next) {
    jsonApi(req, function(err, json) {
        if (err) {
            res.statusCode = 500;
            res.setHeader('content-type', 'text/json');

            res.end(JSON.stringify(err, null, 4));
        }

        res.pageJson = json;

        buildPage(req, res, next);
    });
}

module.exports = loadPage;