var connect = require('connect');
var loadPage = require('./loadPage');
var connectRoute = require('connect-route');

var app = connect();

app.use(connect.logger('dev'));
app.use(connect.static('client/public'));
app.use(connectRoute(function(router) {
    router.get('/:brand/:locale/:page', loadPage);
}));
app.use(function(req, res) {
    res.end(req.url + ' not found.');
});

module.exports = app;