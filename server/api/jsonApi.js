var fs = require('fs');
var path = require('path');

var queryApi = function(req, callback) {
    fs.readFile(path.resolve(path.join('mockApiData', req.params.brand, req.params.locale, req.params.page + '.json')), 'utf8', function(err, data) {
        if (err) {
            callback(err);
        }

        callback(null, JSON.parse(data));
    });
};

module.exports = queryApi;